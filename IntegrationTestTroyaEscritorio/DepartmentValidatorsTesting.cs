﻿using control_tareas_escritorio.Model;
using control_tareas_escritorio.Validators;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.ServiceModel.Channels;
using System.Text;

namespace UnitTestingTroyaEscritorio
{
    [TestClass]
    public class DepartmentValidatorsTesting 
    {

        DepartamentosValidator validationRules = new DepartamentosValidator();
        DepartamentosValidatorDelete validationRulesDelete = new DepartamentosValidatorDelete();


        #region Insert Departament

        [TestMethod]
        public void InsertDepartmentValidateEmptyTesting()
        {
            Departamento departamento = new Departamento();
            var resultados = validationRules.Validate(departamento);

            Assert.IsFalse(resultados.IsValid);        
        }

        [TestMethod]
        public  void InsertDepartamentNameDepartmentValidateTesting()
        {
            Departamento departamento = new Departamento();
            departamento.NombreDepartamento = "Recursos Humanos";

            var resultados = validationRules.Validate(departamento);

            Assert.IsTrue(resultados.IsValid);
        }

        [TestMethod]
        public void InsertDepartmentChar2DepartamentValidateTesting() 
        {
            Departamento departamento = new Departamento();
            departamento.NombreDepartamento = "Re";

            var resultados = validationRules.Validate(departamento);

            Assert.IsFalse(resultados.IsValid);
        } 

        #endregion

        #region Delete Department 


        [TestMethod] 

        public void DeleteDepartmentEmptyIdTesting()
        {         
            string Id = "";
            var resultados = validationRulesDelete.Validate(Id);

            Assert.IsFalse(resultados.IsValid);
        }

        [TestMethod]

        public void DeleteDepartmentNotEmptyIdTesting() 
        {
            Departamento departamento = new Departamento();
            departamento.Id = ObjectId.GenerateNewId();
            var resultados = validationRulesDelete.Validate(departamento.Id.ToString());

            Assert.IsTrue(resultados.IsValid);
        }


        #endregion

        


    }
}
