using control_tareas_escritorio.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace UnitTestingTroyaEscritorio
{
    [TestClass]
    public class HelperTesting
    {

        ILoginHelper helper = new LoginHelper();

        #region HashPassword

        [TestMethod]
        public void HashPasswordWithDataTesting()
        {
            string password = "1234";
            string hashPassword = helper.HashPassword(password);

            Assert.IsNotNull(hashPassword);
        }

        [TestMethod]
        public void HashPasswordWithoutDataTesting()
        {
            string password = "";
            string hashPassword = helper.HashPassword(password);

            Assert.IsNotNull(hashPassword);
        }

        [TestMethod]
        public void HashPassword50CharTesting()
        {
            string password = string.Concat(Enumerable.Repeat("1", 50)); ;
            string hashPassword = helper.HashPassword(password);

            Assert.IsNotNull(hashPassword);
            Assert.AreEqual(password.Length, 50);
        }

        [TestMethod]
        public void HashPasswordCompareTesting()
        {
            string passwordFirst = "1234";
            string passwordSecond = "1234";

            string passwordFirstHashed = helper.HashPassword(passwordFirst);
            string passwordSecondHashed = helper.HashPassword(passwordSecond);

            Assert.AreNotEqual(passwordFirstHashed, passwordSecondHashed);

        }

        [TestMethod]
        public void HashPasswordLargeTesting()
        {
            string passwordFirst = "1234";
            string passwordSecond = "1234";

            string passwordFirstHashed = helper.HashPassword(passwordFirst);
            string passwordSecondHashed = helper.HashPassword(passwordSecond);

            Assert.AreEqual(passwordFirstHashed.Length, passwordSecondHashed.Length);
        }

        #endregion

        #region Validate Password

        [TestMethod]

        public void ValidatePasswordTesting()
        {
            string password = "1234";
            string hashPassword = helper.HashPassword(password);

            bool validedPassword = helper.ValidatePassword(password, hashPassword);

            Assert.IsTrue(validedPassword);
        }

        [TestMethod]
        public void ValidateTwoPasswordCrossingTesting()
        { 
            string passwordfirst  = "1234";
            string passwordSecond = "1234"; 

            string hashPasswordFirst  = helper.HashPassword(passwordfirst);
            string hashPasswordSecond = helper.HashPassword(passwordSecond);

            bool validedPasswordFirst  = helper.ValidatePassword(passwordfirst,  hashPasswordFirst);
            bool validedPasswordSecond = helper.ValidatePassword(passwordSecond, hashPasswordSecond);


            bool validedPasswordCrossFirst  = helper.ValidatePassword(passwordfirst, hashPasswordSecond);
            bool validedPasswordCrossSecond = helper.ValidatePassword(passwordSecond, hashPasswordFirst); 

            Assert.IsTrue(validedPasswordFirst);
            Assert.IsTrue(validedPasswordSecond);
            Assert.IsTrue(validedPasswordCrossFirst);
            Assert.IsTrue(validedPasswordCrossSecond);

        }


        #endregion

        #region EmptyPassword

        [TestMethod]

        public void NotEmptyFieldsTesting()
        {
            string password = "1235";
            string email = "alexander@gmail.com";

            bool IsNotEmpty = helper.EmptyTextBox(email, password);

            Assert.IsTrue(IsNotEmpty);
        }

        [TestMethod]

        public void IsEmptyPasswordFieldTesting()
        {
            string password = "";
            string email = "alexander@gmail.com";

            bool IsNotEmpty = helper.EmptyTextBox(email, password);

            Assert.IsFalse(IsNotEmpty); 
        }

        [TestMethod]

        public void IsEmptyFieldsTesting() 
        {
            string password = "";
            string email = "";

            bool IsNotEmpty = helper.EmptyTextBox(email, password);

            Assert.IsFalse(IsNotEmpty);
        }


        #endregion
    }
}
