﻿using control_tareas_escritorio.Model;
using control_tareas_escritorio.Validators;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;

namespace UnitTestingTroyaEscritorio
{

    [TestClass]
    public class RolesValidatorsTesting
    {

        RolesValidator validationRules = new RolesValidator();
        RolesValidatorDelete validationRulesDelete = new RolesValidatorDelete();

        #region InsertRoles


        [TestMethod]
        public void InsertRolEmptyTesting()
        {
            Roles roles = new Roles();
            var resultado = validationRules.Validate(roles);

            Assert.IsFalse(resultado.IsValid);
        }

        [TestMethod]
        public void InsertRolFullTesting()
        {
            Roles roles = new Roles();
            roles.NombreRol = "Jefe Directo";
            var resultado = validationRules.Validate(roles);

            Assert.IsTrue(resultado.IsValid);

        }

        [TestMethod]
        public void InsertRol2charTesting()
        {
            Roles rol = new Roles( NombreRol:"GO" );
            var resultado = validationRules.Validate(rol);

            Assert.IsFalse(resultado.IsValid);

        }


        #endregion

        #region DeleteRoles

        [TestMethod]
        public void DeleteRolIsEmptyTesting() 
        {
            string Id = "";
            var resultado = validationRulesDelete.Validate(Id);

            Assert.IsFalse(resultado.IsValid);
        }

        [TestMethod]
        public void DeleteRolIsNotEmptyTesting()
        {
            string Id = "123142124";
            var resultado = validationRulesDelete.Validate(Id);

            Assert.IsTrue(resultado.IsValid);
        }
              
        #endregion
    }
}
