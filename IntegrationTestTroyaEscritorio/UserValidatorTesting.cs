﻿using control_tareas_escritorio.Model;
using control_tareas_escritorio.Validators;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace UnitTestingTroyaEscritorio
{
    [TestClass]
    public class UserValidatorTesting
    {
        UsuariosValidator validationRules = new UsuariosValidator();
        UsuariosValidatorDelete validationRulesDelete = new UsuariosValidatorDelete();

        #region InsertUser

        [TestMethod]
        public void InsertUserFullValidatorTesting()
        {
            Usuario usuario    = new Usuario();
            usuario.Nombre     = "Cristopher";
            usuario.Apellido   = "Angulo";
            usuario.Correo     = "cristophe@gmail.com";
            usuario.Contrasena = "@Cristopher2020";

            var resultado = validationRules.Validate(usuario);
            Assert.IsTrue(resultado.IsValid);

        }

        [TestMethod]
        public void InsertUserIsEmptyValidatorTesting()
        {
            Usuario usuario    = new Usuario();

            var resultado = validationRules.Validate(usuario);
            Assert.IsFalse(resultado.IsValid);
        }


        #endregion

        #region DeleteUser


        [TestMethod]
        public void DeleteUserEmptyIdTesting() 
        {
            string Id = "";
            var resultados = validationRulesDelete.Validate(Id);

            Assert.IsFalse(resultados.IsValid);
        }

        [TestMethod]
        public void DeleteUserNotEmptyIdTesting()
        {
            Usuario usuario = new Usuario();
            usuario.Id = ObjectId.GenerateNewId();
            var resultados = validationRulesDelete.Validate(usuario.Id.ToString());

            Assert.IsTrue(resultados.IsValid);
        }



        #endregion
    }
}
