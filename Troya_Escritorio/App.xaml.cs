﻿using control_tareas_escritorio.Observer;
using System;
using System.Windows;

namespace control_tareas_escritorio
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        public static string databaseName = "Contacts.bd";
        public static string folderPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        public static string databasePath = System.IO.Path.Combine(folderPath, databaseName);
        public static  IObserver observerEntities = new ObserverEntities("ControlDepartamento");
    }
}
