﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace control_tareas_escritorio.Cache
{
    public class UserCache
    {

        [PrimaryKey]
        public int Id { get; set; }
        public string Mail   { get; set; }
        public string Password { get; set; }
        public bool IsChecked { get; set; } 


    }
}
