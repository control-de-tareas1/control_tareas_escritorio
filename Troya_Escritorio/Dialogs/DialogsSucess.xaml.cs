﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace control_tareas_escritorio.Dialogs
{
    /// <summary>
    /// Lógica de interacción para DialogsSucess.xaml
    /// </summary>
    public partial class DialogsSucess : MetroWindow
    {
        public DialogsSucess(string mensaje)
        {
            InitializeComponent();
            Exito.Text = mensaje;
        }

        private void Confirm_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
