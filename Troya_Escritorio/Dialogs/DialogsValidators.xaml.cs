﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace control_tareas_escritorio.Dialogs
{
    /// <summary>
    /// Lógica de interacción para DialogsValidators.xaml
    /// </summary>
    public partial class DialogsValidators : MetroWindow
    {
        public DialogsValidators(string mensaje)
        {
            InitializeComponent();
            Error.Text = mensaje; 
        }

        private void CloseError_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
