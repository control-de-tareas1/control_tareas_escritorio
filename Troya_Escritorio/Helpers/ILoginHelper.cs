﻿using System;
using System.Collections.Generic;
using System.Text;

namespace control_tareas_escritorio.Helpers
{
    public interface ILoginHelper
    {
        bool EmptyTextBox(string txtcorreo, string txtpassword);
        string HashPassword(string passsword);
        bool ValidatePassword(string NormalPassword, string HashingPassword);
        string DesencryptarOrEncryptar(string password);
    }
}
