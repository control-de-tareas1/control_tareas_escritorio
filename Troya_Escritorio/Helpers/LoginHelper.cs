﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace control_tareas_escritorio.Helpers
{
    public class LoginHelper : ILoginHelper
    {
        public bool EmptyTextBox(string txtcorreo, string txtpassword)
        {
            try
            {
                if (txtcorreo != string.Empty && txtpassword != string.Empty)
                {
                    return true;
                }

                return false;
            }
            catch (Exception)
            {

                return false; 
            }

        }

        public string HashPassword(string passsword)
        {
            try
            {
                return BCrypt.Net.BCrypt.HashPassword(passsword);
            }
            catch (Exception)
            {

                return passsword;
            }

        }

        public bool ValidatePassword(string NormalPassword, string HashingPassword)
        {
            try
            {
                bool isValidPassword = BCrypt.Net.BCrypt.Verify(NormalPassword, HashingPassword);
               
                if (isValidPassword)
                {
                    return true;
                }

                return false;
            }
            catch (Exception)
            {

                
                return false;


            }

        }

        public string DesencryptarOrEncryptar(string password )
        {
            try
            {
                 var pass = BCrypt.Net.BCrypt.InterrogateHash(password);
                 return password; 
            }
            catch (Exception)
            {

                return "";

            }

        }
    }
}
