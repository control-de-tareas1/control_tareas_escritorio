﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace control_tareas_escritorio.Model
{
    public class Departamento
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId Id { get; set; }

        [BsonElement("NombreDepartamento")]
        public string NombreDepartamento { get; set; }

        [BsonElement("DepartamentoJefe")]
        public string DepartamentoJefe { get; set; }

        public Departamento()
        {

        }

        public Departamento(ObjectId Id, string NombreDepartamento)
        {
            this.Id = Id;
            this.NombreDepartamento = NombreDepartamento;

        }

        public Departamento(ObjectId Id, string NombreDepartamento, string DepartamentoJefe)
        {
            this.Id = Id;
            this.NombreDepartamento = NombreDepartamento;
            this.DepartamentoJefe = DepartamentoJefe;

        }



    }
}
