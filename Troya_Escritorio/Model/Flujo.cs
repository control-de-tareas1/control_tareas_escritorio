﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace control_tareas_escritorio.Model
{
    public class Flujo
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId Id { get; set; }

        [BsonElement("NombreFlujo")]
        public string NombreFlujo { get; set; }

        [BsonElement("Tareas")]
        public List<Tarea> Tareas { get; set; }


        public Flujo(ObjectId id , string nombreFlujo , List<Tarea> tareas )
        {
            Id = id;
            NombreFlujo = nombreFlujo;
            Tareas = tareas;
        }

        public Flujo()
        {

        }

    }
}
