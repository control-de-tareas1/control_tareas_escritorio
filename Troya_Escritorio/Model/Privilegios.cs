﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace control_tareas_escritorio.Model
{
    public class Privilegios
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId Id { get; set; }

        [BsonElement("NombrePrivilegio")]
        public string NombrePrivilegio { get; set; }

        public bool isChecked { get; set; }

        public Privilegios(string Nombre)
        {
            this.NombrePrivilegio = Nombre;
            this.isChecked = true;
        }

        public Privilegios(string Nombre, ObjectId Id)
        {
            this.Id = Id;
            this.NombrePrivilegio = Nombre;
            this.isChecked = true;
        }

        public Privilegios()
        {
            this.isChecked = true;

        }


    }
}
