﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace control_tareas_escritorio.Model
{
    public class Roles
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId Id { get; set; }

        [BsonElement("NombreRol")]
        public string NombreRol { get; set; }

        [BsonElement("Privilegios")]
        public ArrayList Privilegios { get; set; }

        public Roles()
        {

        }

        public Roles(ObjectId Id, string NombreRol)
        {
            this.NombreRol = NombreRol;
            this.Id = Id;
        }

        public Roles(string NombreRol)
        {
            this.NombreRol = NombreRol;
        }

        public Roles(ObjectId Id, string NombreRol, ArrayList privilegios)
        {
            this.NombreRol = NombreRol;
            this.Id = Id;
            this.Privilegios = privilegios;
        }



    }



}
