﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace control_tareas_escritorio.Model
{
    public class Tarea
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        
        public ObjectId Id { get; set; }

        public string NombreTarea { get; set; }

        public string Descripcion { get; set; }

        public  string Justificacion { get; set; }

        public string Responsable { get; set; }

        public DateTime FechaInicio { get; set; }

        public DateTime FechaTermino { get; set; }

        public string Problema { get; set; }

        public bool AceptarRechazar { get; set; }


        public Tarea()
        {

        }


        public Tarea(string nombreTarea)
        {
            Id = ObjectId.GenerateNewId();
            NombreTarea = nombreTarea;
            Descripcion = "";
            Justificacion = "";
            Responsable = "";
            FechaInicio = DateTime.Now;
            FechaTermino = DateTime.Now;
            Problema = "";
            AceptarRechazar = false;
        }
    }
}
