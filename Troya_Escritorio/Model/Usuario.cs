﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace control_tareas_escritorio.Model
{
    public class Usuario
    {

        public Usuario()
        {

        }
        public Usuario( string nombre, string apellido, string correo, string contrasena)
        {
            this.Nombre = nombre;
            this.Apellido = apellido;
            this.Contrasena = contrasena;
            this.Correo = correo; 
        }
        public Usuario(string nombre, string apellido, string correo, string contrasena, string departamento)
        {
            this.Nombre = nombre;
            this.Apellido = apellido;
            this.Contrasena = contrasena;
            this.Correo = correo;
            this.Departamento = departamento;
        }
        public Usuario(string nombre, string apellido, string correo, string contrasena, string departamento, Roles roles)
        {
            this.Nombre = nombre;
            this.Apellido = apellido;
            this.Contrasena = contrasena;
            this.Correo = correo;
            this.Departamento = departamento;
            this.Roles = roles;
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId Id { get; set; }

        [BsonElement("NombreUsuario")]
        public string Nombre { get; set; }

        [BsonElement("ApellidoUsuario")]
        public string Apellido{ get; set; }

        [BsonElement("ContrasenaUsuario")]
        public string Contrasena { get; set; }

        [BsonElement("CorreoUsuario")]
        public string Correo { get; set; }

        [BsonElement("DepartamentoUsuario")]
        public string Departamento { get; set; }

        [BsonElement("Roles")]
        public Roles Roles { get; set; }

    }
}
