﻿using control_tareas_escritorio.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace control_tareas_escritorio.Observer
{
    public interface IObserver
    {

        void ActualizarWindow();

    }
}
