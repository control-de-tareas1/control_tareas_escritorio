﻿using System;
using System.Collections.Generic;
using System.Text;

namespace control_tareas_escritorio.Observer
{

    /// <summary>
    /// Sujeto que cuando cambia su estado notifica
    /// </summary>
    public interface ISubject
    {
        void Register(IObserver observer);
        void Unregister(IObserver observer);
        void Notify();

    }
}
