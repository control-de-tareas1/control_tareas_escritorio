﻿using control_tareas_escritorio.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace control_tareas_escritorio.Observer
{
    public class SubjectEntities : ISubject
    {

        // INIT
        private List<IObserver> _observers = new List<IObserver>();
        private Departamento departamento;
        private Usuario usuario;
        private Roles roles;


        // CTOR

        public SubjectEntities(Departamento departamento)
        {
            this.Departamento = departamento;
        }
        public SubjectEntities(Usuario usuario)
        {
            this.Usuario = usuario;
        }
        public SubjectEntities(Roles roles)
        {
            this.roles = roles;
        }
       
        
        // PROPS
        public Departamento Departamento { 
            get => departamento;
            set {
                if (departamento != value)
                {
                    departamento = value;
                    Notify();
                }
            }       
        }
        public Usuario Usuario
        {
            get => usuario;
            set
            {
                if (usuario != value)
                {
                    usuario = value;
                    Notify();
                }
            }
        }
        public Roles Roles
        {
            get => roles;
            set
            {
                if (roles != value)
                {
                    roles = value;
                    Notify();
                }
            }
        }

        // NOTIFICAR A CADA SUSCRIPTOR
        public void Notify()
        {
            _observers.ForEach(p => p.ActualizarWindow());
        }
        // SUSCRIBIRSE
        public void Register(IObserver observer)
        {
            _observers.Add(observer);
        }

        // DESUSCRIBIRSE
        public void Unregister(IObserver observer)
        {
            _observers.Remove(observer);
        }
    }
}
