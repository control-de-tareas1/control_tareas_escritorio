﻿using control_tareas_escritorio.Context;
using control_tareas_escritorio.Model;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace control_tareas_escritorio.Repository.DepartamentoRepositorio
{
    public class DepartamentoColeccion : IDepartamentoColeccion
    {
        internal MongoDBContexto contexto = new MongoDBContexto("ControlTareas");

        private IMongoCollection<Departamento> Coleccion;

        public DepartamentoColeccion()
        {
            Coleccion = contexto.db.GetCollection<Departamento>("departamentos");
        }
        public async Task DeleteDepartment(string id)
        {           
            var filter = Builders<Departamento>.Filter.Eq(s => s.Id, new ObjectId(id));
            await Coleccion.DeleteOneAsync(filter);
        }

        public async  Task<List<Departamento>> GetAllDepartments()
        {
            return await Coleccion.FindAsync(new BsonDocument()).Result.ToListAsync();
            // return await Coleccion.AsQueryable().ToListAsync<Departamento>();
        }

        public async  Task<ObservableCollection<Departamento>> GetAllDepartmentsAutomatic()
        {
            var response = await Coleccion.AsQueryable().ToListAsync<Departamento>();
            ObservableCollection<Departamento> departments = new ObservableCollection<Departamento>(response);
            return departments;
        }

        public async Task<List<Departamento>> GetAllDepartmentsWithoutNulls()
        {
            var filter = Builders<Departamento>.Filter.Exists(s => s.DepartamentoJefe )!=null;
            return await Coleccion.FindAsync(new BsonDocument(filter)).Result.ToListAsync();
        }

        public async Task InsertDepartment(Departamento departamento)
        {
            await Coleccion.InsertOneAsync(departamento);
        }

        public async Task UpdateDepartment(Departamento departamento)
        {
            var filter = Builders<Departamento>.Filter.Eq(s => s.Id, departamento.Id);
            await Coleccion.ReplaceOneAsync(filter, departamento);

        }
    }
}
