﻿using control_tareas_escritorio.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace control_tareas_escritorio.Repository.DepartamentoRepositorio
{
    public interface IDepartamentoColeccion
    {
        Task InsertDepartment(Departamento departamento);
        Task UpdateDepartment(Departamento departamento);
        Task DeleteDepartment(string id);
        Task<List<Departamento>> GetAllDepartments();
        Task<ObservableCollection<Departamento>> GetAllDepartmentsAutomatic();
        Task<List<Departamento>> GetAllDepartmentsWithoutNulls();

    }
}
