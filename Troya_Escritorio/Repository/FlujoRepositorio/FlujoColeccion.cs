﻿using control_tareas_escritorio.Context;
using control_tareas_escritorio.Model;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace control_tareas_escritorio.Repository.FlujoRepositorio
{
    public class FlujoColeccion : IFlujoColeccion
    {
        internal MongoDBContexto contexto = new MongoDBContexto("ControlTareas");

        private IMongoCollection<Flujo> Coleccion;

        public FlujoColeccion()
        {
            Coleccion = contexto.db.GetCollection<Flujo>("flujoTareas");
        }

        public async Task DeleteFlujo(string id)
        {
            var filter = Builders<Flujo>.Filter.Eq(s => s.Id, new ObjectId(id));
            await Coleccion.DeleteOneAsync(filter);
        }

        public async Task<List<Flujo>> GetAllFlujo()
        {
            return await Coleccion.FindAsync(new BsonDocument()).Result.ToListAsync();
        }

        public async  Task InsertFlujo(Flujo flujo)
        {
            await Coleccion.InsertOneAsync(flujo);
        }

        public async Task UpdateFlujo(Flujo flujo)
        {
            var filter = Builders<Flujo>.Filter.Eq(s => s.Id, flujo.Id);
            await Coleccion.ReplaceOneAsync(filter, flujo);
        }
    }
}
