﻿using control_tareas_escritorio.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace control_tareas_escritorio.Repository.FlujoRepositorio
{
    public interface IFlujoColeccion
    {
        Task InsertFlujo(Flujo flujo);
        Task UpdateFlujo(Flujo flujo);
        Task DeleteFlujo(string id);
        Task<List<Flujo>> GetAllFlujo();


    }
}
