﻿using control_tareas_escritorio.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace control_tareas_escritorio.Repository.PrivilegiosRepositorio
{
    public interface IPrivilegiosColeccion
    {
        Task<List<Privilegios>> GetAllPrivilegios();
        Task InsertPrivilegio(Privilegios privilegio);
        Task UpdatePrivilegio(Privilegios privilegio);
        Task DeletePrivilegio(string id);
        Task<ObservableCollection<Privilegios>> GetAllPrivilegiossAutomatic(); 



    }
}
