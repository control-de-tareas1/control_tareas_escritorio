﻿using control_tareas_escritorio.Context;
using control_tareas_escritorio.Model;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace control_tareas_escritorio.Repository.PrivilegiosRepositorio
{
    public class PrivilegiosColeccion : IPrivilegiosColeccion
    {

        internal MongoDBContexto contexto = new MongoDBContexto("ControlTareas");

        private IMongoCollection<Privilegios> Coleccion;
        public PrivilegiosColeccion()
        {
            Coleccion = contexto.db.GetCollection<Privilegios>("privilegios");
        }

        public async Task DeletePrivilegio(string id)
        {

            var filter = Builders<Privilegios>.Filter.Eq(s => s.Id, new ObjectId(id));
            await Coleccion.DeleteOneAsync(filter);
        }

        public async  Task<List<Privilegios>> GetAllPrivilegios()
        {
            return await Coleccion.FindAsync(new BsonDocument()).Result.ToListAsync();
        }

        public async Task<ObservableCollection<Privilegios>> GetAllPrivilegiossAutomatic()
        {
            var response = await Coleccion.AsQueryable().ToListAsync<Privilegios>();
            ObservableCollection<Privilegios> privilegios = new ObservableCollection<Privilegios>(response);
            return privilegios;
        }

        public async Task InsertPrivilegio(Privilegios privilegio)
        {
            await Coleccion.InsertOneAsync(privilegio);
        }

        public async Task UpdatePrivilegio(Privilegios privilegio)
        {
            var filter = Builders<Privilegios>.Filter.Eq(s => s.Id, privilegio.Id);
            await Coleccion.ReplaceOneAsync(filter, privilegio);
        }
    }
}
