﻿using control_tareas_escritorio.Model;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace control_tareas_escritorio.Repository.RolesRepositorio
{
    public interface IRolesColeccion
    {
        Task InsertRol(Roles roles);
        Task UpdateRol(Roles roles);
        Task DeleteRol(string id);
        Task<List<Roles>> GetAllRol();
        Task<Roles> Lookup( ObjectId Id );
        Task<ObservableCollection<Roles>> GetAllRolesAutomatic(); 
    }
}
