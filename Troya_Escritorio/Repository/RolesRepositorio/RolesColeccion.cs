﻿using control_tareas_escritorio.Context;
using control_tareas_escritorio.Model;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace control_tareas_escritorio.Repository.RolesRepositorio
{
    public class RolesColeccion : IRolesColeccion
    {
        internal MongoDBContexto contexto = new MongoDBContexto("ControlTareas");

        private IMongoCollection<Roles> mongoCollection;

        public RolesColeccion()
        {
            mongoCollection = contexto.db.GetCollection<Roles>("roles");

        }
        public async Task DeleteRol(string id)
        {
            var filter = Builders<Roles>.Filter.Eq(s => s.Id, new ObjectId(id));
            await mongoCollection.DeleteOneAsync(filter);

        }

        public async Task<List<Roles>> GetAllRol()
        {
            return await mongoCollection.FindAsync(new BsonDocument()).Result.ToListAsync();
        }

        public async Task<ObservableCollection<Roles>> GetAllRolesAutomatic()
        {
            var response = await mongoCollection.AsQueryable().ToListAsync<Roles>();
            ObservableCollection<Roles> roles = new ObservableCollection<Roles>(response); 
            return roles;
        }

        public async Task InsertRol(Roles roles)
        {
            await mongoCollection.InsertOneAsync(roles);
        }

        public async Task<Roles> Lookup( ObjectId  Id)
        {
            return await mongoCollection.FindAsync(new BsonDocument { { "_id", Id } }).Result.FirstAsync();
        }

        public async Task UpdateRol(Roles roles)
        {
            var filter = Builders<Roles>.Filter.Eq(s => s.Id, roles.Id);
            await mongoCollection.ReplaceOneAsync(filter, roles);

        }


    }
}
