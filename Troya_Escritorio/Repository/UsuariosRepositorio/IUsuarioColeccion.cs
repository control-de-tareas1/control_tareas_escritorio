﻿using control_tareas_escritorio.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace control_tareas_escritorio.Repository.UsuariosRepositorio
{
    public  interface IUsuarioColeccion
    {
        Task InsertUser(Usuario usuario);
        Task UpdateUser(Usuario usuario);
        Task DeleteUser(string id);
        Task<List<Usuario>> GetAllUsers();
        Task<Usuario> Authentication(string mail);
        Task<ObservableCollection<Usuario>> GetAllUsuariosAutomatic(); 


    }
}
