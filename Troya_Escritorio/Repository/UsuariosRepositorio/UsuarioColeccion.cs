﻿using control_tareas_escritorio.Context;
using control_tareas_escritorio.Model;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace control_tareas_escritorio.Repository.UsuariosRepositorio
{
    public class UsuarioColeccion : IUsuarioColeccion
    {
        internal MongoDBContexto contexto = new MongoDBContexto("ControlTareas");

        private IMongoCollection<Usuario> Coleccion;

        public UsuarioColeccion()
        {
            Coleccion = contexto.db.GetCollection<Usuario>("usuarios");
        }

        public async Task<Usuario> Authentication(string mail)
        {
            var filter = Builders<Usuario>.Filter.Where(p => p.Correo == mail);
            return await Coleccion.FindAsync(filter).Result.FirstOrDefaultAsync(); 

        }

        public async Task DeleteUser(string id)
        {
            var filter = Builders<Usuario>.Filter.Eq(s => s.Id, new ObjectId(id));
            await Coleccion.DeleteOneAsync(filter);
        }

        public async Task<List<Usuario>> GetAllUsers()
        {
            return await Coleccion.FindAsync(new BsonDocument()).Result.ToListAsync();
        }

        public async Task<ObservableCollection<Usuario>> GetAllUsuariosAutomatic()
        {
            var response = await Coleccion.AsQueryable().ToListAsync<Usuario>();
            ObservableCollection<Usuario> usuarios = new ObservableCollection<Usuario>(response);
            return usuarios;
        }

        public async Task InsertUser(Usuario usuario)
        {
            await Coleccion.InsertOneAsync(usuario);
        }

        public  async Task UpdateUser(Usuario usuario)
        {
            var filter = Builders<Usuario>.Filter.Eq(s => s.Id, usuario.Id);
            await Coleccion.ReplaceOneAsync(filter, usuario);
        }
    }
}
