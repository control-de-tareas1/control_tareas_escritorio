﻿using control_tareas_escritorio.Helpers;
using control_tareas_escritorio.Model;
using control_tareas_escritorio.Observer;
using control_tareas_escritorio.Repository.DepartamentoRepositorio;
using control_tareas_escritorio.Repository.PrivilegiosRepositorio;
using control_tareas_escritorio.Repository.RolesRepositorio;
using control_tareas_escritorio.Repository.UsuariosRepositorio;
using LiveCharts;
using LiveCharts.Defaults;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Media;

namespace control_tareas_escritorio.UserControls
{

    public partial class DashboardControl : UserControl
    {

        List<Privilegios> privilegios;
        List<Usuario> usuarios;
        List<Departamento> departamentos;
        List<Roles> roles;
        Random rand = new Random();

        public static DashboardControl instance;

        public static DashboardControl GetInstance()
        {
            if (instance == null)
            {
                instance = new DashboardControl();

            }
            return instance;
        }

        public DashboardControl()
        {
            InitializeComponent();
            instance = this; 

            GetAllDepartaments();
            GetAllRoles();
            GetAllUsers();
            GetAllPrivilegies();
            BuildCounts();
            BuildLabels();


            Formatter = value => value.ToString("N");
            PointLabel = chartPoint =>
               string.Format("{0} ({1:P})", chartPoint.Y, chartPoint.Participation);
            FillPieChart();
            CreateCollectionDonuts();
            DataContext = this;
      
        
        }

        #region Gráfico de barras
        public SeriesCollection SeriesCollection { get; set; }
        public string[] Labels { get; set; }
        public Func<double, string> Formatter { get; set; }

        #region Dibujar Data

        public void  BuildCounts()
        {
            SeriesCollection = new SeriesCollection
            {
                new ColumnSeries
                {
                    Title = DateTime.Now.Year.ToString(),
                    Values = new ChartValues<double> { usuarios.Count, roles.Count, departamentos.Count, privilegios.Count }
                }
            };

        }

        public void BuildLabels()
        {
            Labels = new[] { "Usuarios", "Roles", "Departamentos", "Privilegios" };
            
        }


        #endregion

        #region Obtener Data

        private async void GetAllPrivilegies()
        {
            IPrivilegiosColeccion privilegiosColeccion = new PrivilegiosColeccion();
            privilegios = await privilegiosColeccion.GetAllPrivilegios();
        }
        private async void GetAllUsers()
        {

            IUsuarioColeccion usuarioColeccion = new UsuarioColeccion();
            usuarios = await  usuarioColeccion.GetAllUsers();           
        }
        private async void GetAllRoles()
        {
            
            IRolesColeccion rolesColeccion = new RolesColeccion();
            roles = await rolesColeccion.GetAllRol();
        }
        public void GetAllDepartaments()
        {
            Dispatcher.Invoke(async () =>
            {

                IDepartamentoColeccion departamentoColeccion = new DepartamentoColeccion();
                var departamentoss = await departamentoColeccion.GetAllDepartments();
                this.departamentos = departamentoss;

              
            });
        }



        #endregion



        #endregion


        #region Gráfico de Tortas




        #region Dibujar Data

        public Func<ChartPoint, string> PointLabel { get; set; }
        private void FillPieChart()
        {           
            foreach (Departamento departamento in departamentos)
            {

                var usuario = usuarios
                    .Where(s => s.Departamento == departamento.NombreDepartamento);

                PieSeries pieSeries = new PieSeries
                {
                    Title = departamento.NombreDepartamento,
                    Values = new ChartValues<Double> { usuario.Count() },
                    DataLabels = true,
                    LabelPoint = PointLabel,
                    StrokeThickness = 2,
                    Fill = new SolidColorBrush(Color.FromRgb((byte)rand.Next(0, 256), (byte)rand.Next(0, 256), (byte)rand.Next(0, 256)))
                };
                PieChartDepartments.Series.Add(pieSeries);
            }               
                

        }       
        private void Chart_OnDataClick(object sender, ChartPoint chartpoint)
        {
            var chart = (LiveCharts.Wpf.PieChart)chartpoint.ChartView;

            //clear selected slice.
            foreach (PieSeries series in chart.Series)
                series.PushOut = 0;

            var selectedSeries = (PieSeries)chartpoint.SeriesView;
            selectedSeries.PushOut = 8;
        }

        #endregion


        #endregion


        #region Gráfico de Donas

        private void CreateCollectionDonuts()
        {

            foreach (Roles rol in roles)
            {
                int privilegios = rol.Privilegios.Count;
                PieSeries pieSeries = new PieSeries
                {
                    Title = rol.NombreRol,
                    Values = new ChartValues<ObservableValue> { new ObservableValue(privilegios) },
                    DataLabels = true
                };

                PieChartRoles.Series.Add(pieSeries);
              
            }
         

        }

        #endregion
    }
}
