﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using control_tareas_escritorio.Dialogs;
using control_tareas_escritorio.Model;
using control_tareas_escritorio.Observer;
using control_tareas_escritorio.Repository.DepartamentoRepositorio;
using control_tareas_escritorio.Validators;
using MongoDB.Bson;

namespace control_tareas_escritorio.UserControls
{
    /// <summary>
    /// Lógica de interacción para Departamento.xaml
    /// </summary>
    public partial class DepartamentoControl : UserControl
    {
        BindingList<string> errores = new BindingList<string>();
        DepartamentosValidator validationRules = new DepartamentosValidator();
        DepartamentosValidatorDelete validationRulesDelete = new DepartamentosValidatorDelete();
        private ObservableCollection<Departamento> departamentosObservable;

        public static DepartamentoControl instance;
        public static DepartamentoControl GetInstance()
        {
            if (instance == null)
            {
                instance = new DepartamentoControl();

            }
            return instance;
        }
        public  DepartamentoControl()
        {
            InitializeComponent();
            instance = this;
            RellenarComboBox();
            RellenarDataGrid();
            DtDepartamento.ItemsSource = departamentosObservable;

        }

        #region Controles
        private async void RellenarComboBox()
        {
            try
            {
                IEnumerable departamentos = new List<Departamento>();

                await Task.Run(async () =>
                {
                    IDepartamentoColeccion db = new DepartamentoColeccion();
                    departamentos = await db.GetAllDepartmentsWithoutNulls();
                    return departamentos;
                });


                cmBoxDepartamentosJefes.ItemsSource = departamentos;
                cmBoxDepartamentosJefes.DisplayMemberPath = "NombreDepartamento";
            }
            catch (Exception ex)
            {

                MessageBox.Show($"{ex.Message}");
            }

        }

        private async  void  RellenarDataGrid()
        {
            try
            {
                //IEnumerable departamentos = new List<Departamento>();

                await Task.Run(async () =>
                {
                    IDepartamentoColeccion db = new DepartamentoColeccion();
                    departamentosObservable = await db.GetAllDepartmentsAutomatic();
                    return departamentosObservable;
                });
                DtDepartamento.ItemsSource = departamentosObservable;
            }
            catch (Exception ex)
            {

                MessageBox.Show($"{ex.Message}");
            }

            


        }

        private async void DtDepartamento_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                await Task.Delay(TimeSpan.FromSeconds(0.5));

                // cmboDepartamento.Text = Nulos.DepartamentoJefe;
                
                Departamento Nulos = (Departamento)DtDepartamento.SelectedItem;
        

                if (Nulos != null)
                {
                    txtBoxIdDepartamento.Text = Nulos.Id.ToString();
                    txtBoxNombreDepartamento.Text = Nulos.NombreDepartamento;
                    cmBoxDepartamentosJefes.Text = Nulos.DepartamentoJefe;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show($"{ex.Message}");
            }

        }

        private void LimpiarControles(){
            txtBoxNombreDepartamento.Text  = "";
            txtBoxIdDepartamento.Text      = "";
            cmBoxDepartamentosJefes.Text   = "";
        }
       
        private void Boton_LimpiarDepartamento(object sender, RoutedEventArgs e)
        {
            LimpiarControles();
        }

        private void TxtBuscarDepartamento_KeyUp(object sender, System.Windows.Input.KeyEventArgs e) {

            if (txtBuscarDepartamento.Text.Equals(""))
            {
                RellenarDataGrid();
            }

            var filtered = departamentosObservable
                .Where( x => x.NombreDepartamento.ToLower().Trim() == txtBuscarDepartamento.Text.ToLower().Trim() 
                || x.DepartamentoJefe.ToLower().Trim() == txtBuscarDepartamento.Text.ToLower().Trim())
                .ToList();

            DtDepartamento.ItemsSource = filtered;
        }

        private void Buton_Refrescar(object sender, RoutedEventArgs e)
        {
            RellenarDataGrid();
        }

        #endregion

        #region CRUD

        private async void Buton_AgregarDepartamento(object sender, RoutedEventArgs e)
        {
            try
            {
                errores.Clear();

                IDepartamentoColeccion db = new DepartamentoColeccion();
                Departamento departamento = new Departamento();
                departamento.NombreDepartamento = txtBoxNombreDepartamento.Text;
                departamento.DepartamentoJefe = (cmBoxDepartamentosJefes.Text != null) ? cmBoxDepartamentosJefes.Text : "";


                var resultados = validationRules.Validate(departamento);

                if (resultados.IsValid == false)
                {
                    foreach (var item in resultados.Errors)
                    {
                        DialogsValidators dialogsValidators = new DialogsValidators(item.ErrorMessage);
                        dialogsValidators.ShowDialog();
                    }
                }
                else
                {
                    await db.InsertDepartment(departamento);
                    departamentosObservable.Add(departamento);

                    DialogsSucess dialogsSucess = new DialogsSucess($"El Departamento {departamento.NombreDepartamento} ha sido registrado");
                    dialogsSucess.ShowDialog();

                    RellenarComboBox();
                    LimpiarControles();

                    ISubject subjectEntities = new SubjectEntities(departamento);
                    subjectEntities.Register(App.observerEntities);
                    App.observerEntities.ActualizarWindow();

                }

            }
            catch (Exception ex)
            {

                DialogsValidators dialogsValidators = new DialogsValidators(ex.Message);
                dialogsValidators.ShowDialog();
            }

        }

        private async void Boton_ActualizarDepartamento(object sender, RoutedEventArgs e)
        {
            try
            {
                errores.Clear();


                IDepartamentoColeccion db = new DepartamentoColeccion();
                Departamento departamento = new Departamento();

                if (txtBoxIdDepartamento.Text.Length > 0)
                {
                    departamento.Id = ObjectId.Parse(txtBoxIdDepartamento.Text);
                }
                else
                {
                    DialogsValidators dialogsValidators = new DialogsValidators("El Id no puede estar Vacío");
                    dialogsValidators.ShowDialog();
                }
     
               
                
                departamento.NombreDepartamento = txtBoxNombreDepartamento.Text;
                departamento.DepartamentoJefe = (cmBoxDepartamentosJefes.Text != null) ? cmBoxDepartamentosJefes.Text : "";

                var resultados = validationRules.Validate(departamento);

                if (resultados.IsValid == false)
                {
                    foreach (var item in resultados.Errors)
                    {
                        DialogsValidators dialogsValidators = new DialogsValidators(item.ErrorMessage);
                        dialogsValidators.ShowDialog();
                    }
                }
                else
                {
                    await db.UpdateDepartment(departamento);
                    var d = departamentosObservable.Where(x => x.Id == ObjectId.Parse(txtBoxIdDepartamento.Text)).First();
                    departamentosObservable.Remove(d);
                    departamentosObservable.Add(departamento);

                    DialogsSucess dialogsSucess = new DialogsSucess($"El Departamento {departamento.NombreDepartamento} ha sido actualizado");
                    dialogsSucess.ShowDialog();

                    RellenarComboBox();
                    LimpiarControles();

                    ISubject subjectEntities = new SubjectEntities(departamento);
                    subjectEntities.Register(App.observerEntities);
                    App.observerEntities.ActualizarWindow();
                }


            }
            catch (Exception ex)
            {

                  DialogsValidators dialogsValidators = new DialogsValidators(ex.Message);
                  dialogsValidators.ShowDialog();
                

            }
        }

        private async void Boton_EliminarDepartamento(object sender, RoutedEventArgs e)
        {
            try
            {
                errores.Clear();
                IDepartamentoColeccion db = new DepartamentoColeccion();
                var resultados = validationRulesDelete.Validate(txtBoxIdDepartamento.Text);

                if (resultados.IsValid == false)
                {
                    foreach (var item in resultados.Errors)
                    {
                        DialogsValidators dialogsValidators = new DialogsValidators(item.ErrorMessage);
                        dialogsValidators.ShowDialog();
                    }
                }
                else
                {
                    await db.DeleteDepartment(txtBoxIdDepartamento.Text);
                    var d = departamentosObservable.Where(x => x.Id == ObjectId.Parse(txtBoxIdDepartamento.Text)).First();
                    departamentosObservable.Remove(d);

                    DialogsSucess dialogsSucess = new DialogsSucess($"El Departamento {d.NombreDepartamento} ha sido borrado");
                    dialogsSucess.ShowDialog();

                    RellenarComboBox();
                    LimpiarControles();

                    ISubject subjectEntities = new SubjectEntities(d);
                    subjectEntities.Register(App.observerEntities);
                    App.observerEntities.ActualizarWindow();
                }

            }
            catch (Exception ex)
            {

                DialogsValidators dialogsValidators = new DialogsValidators(ex.Message);
                dialogsValidators.ShowDialog();
            }


        }

        #endregion

    }
}
