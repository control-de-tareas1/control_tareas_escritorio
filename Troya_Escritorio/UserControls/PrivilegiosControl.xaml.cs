﻿using control_tareas_escritorio.Model;
using control_tareas_escritorio.Repository.PrivilegiosRepositorio;
using control_tareas_escritorio.Validators;
using MongoDB.Bson;
using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace control_tareas_escritorio.UserControls
{
    /// <summary>
    /// Lógica de interacción para PrivilegiosControl.xaml
    /// </summary>
    public partial class PrivilegiosControl : UserControl
    {

        BindingList<string> errores = new BindingList<string>();
  
        
        public PrivilegiosControl()
        {

            InitializeComponent();
            RellenarDatagrid();
        }

        private async void Buton_AgregarPrivilegio(object sender, RoutedEventArgs e)
        {
            errores.Clear();

            IPrivilegiosColeccion db = new PrivilegiosColeccion();
            Privilegios privilegios = new Privilegios();
            privilegios.NombrePrivilegio = txtBoxNombrePrivilegio.Text;

            PrivilegiosValidator validationRules = new PrivilegiosValidator();
            var resultados = validationRules.Validate(privilegios);

            if (resultados.IsValid == false)
            {
                foreach (var item in resultados.Errors) 
                {
                    MessageBox.Show(item.ErrorMessage,"Error");
                }
            }
            else
            {
                await db.InsertPrivilegio(privilegios);
               

            }

        }

        private async void Boton_ActualizarPrivilegio(object sender, RoutedEventArgs e)
        {
            IPrivilegiosColeccion db = new PrivilegiosColeccion();
            Privilegios privilegios = new Privilegios();
            privilegios.Id = ObjectId.Parse(txtBoxIdPrivilegio.Text);
            privilegios.NombrePrivilegio = txtBoxNombrePrivilegio.Text;
            await db.UpdatePrivilegio(privilegios);
            //RellenarDatagrid();
        }

        private async void Boton_EliminarPrivilegio(object sender, RoutedEventArgs e)
        {
            IPrivilegiosColeccion db = new PrivilegiosColeccion();
            await db.DeletePrivilegio(txtBoxIdPrivilegio.Text);
            //RellenarDatagrid();
        }

        private async void DtPrivilegios_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                await Task.Delay(TimeSpan.FromSeconds(0.5));

                Privilegios Nulos = (Privilegios)DtPrivilegios.SelectedItem;


                if (Nulos != null)
                {
                    txtBoxIdPrivilegio.Text = Nulos.Id.ToString();
                    txtBoxNombrePrivilegio.Text = Nulos.NombrePrivilegio;
                }

            }
            catch (System.Exception)
            {

                throw;
            }
        }

        private void RellenarDatagrid()
        {
            //DtPrivilegios.ItemsSource = viewModel1.ListaScreenPrivilegios;

        }
     


    }
}

