﻿using control_tareas_escritorio.Dialogs;
using control_tareas_escritorio.Model;
using control_tareas_escritorio.Observer;
using control_tareas_escritorio.Repository.PrivilegiosRepositorio;
using control_tareas_escritorio.Repository.RolesRepositorio;
using control_tareas_escritorio.Validators;
using MongoDB.Bson;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace control_tareas_escritorio.UserControls
{
    /// <summary>
    /// Lógica de interacción para RolesControl.xaml
    /// </summary>
    public partial class RolesControl : UserControl
    {

        BindingList<string> errores = new BindingList<string>();
        RolesValidator validationRules = new RolesValidator();
        RolesValidatorDelete validationRulesDelete = new RolesValidatorDelete();
        ObservableCollection<Roles> rolesObservable;

        public static RolesControl instance;
        public static RolesControl GetInstance()
        {
            if (instance == null)
            {
                instance = new RolesControl();

            }
            return instance;
        }
        public RolesControl()
        {
            InitializeComponent();
            instance = this; 
            RellenarComboBox();
            RellenarGrid();
        }

        #region Controles
        private void LimpiarControles()
        {

            txtBoxIdRol.Text = "";
            txtBoxNombreRol.Text = "";
            cmBoxPrivilegios.Text = "";
        }
        private async void RellenarGrid()
        {
            try
            {
               // IEnumerable privilegios = new List<Roles>();

                await Task.Run(async () =>
                {
                    IRolesColeccion db = new RolesColeccion();
                    rolesObservable = await db.GetAllRolesAutomatic();
                    return rolesObservable;
                });
                DtRoles.ItemsSource = rolesObservable;
            }
            catch (Exception ex)
            {

                MessageBox.Show($"{ex}");
            }


        }
        private async void RellenarComboBox()
        {
            try
            {
                IEnumerable privilegios = new List<Privilegios>();

                await Task.Run(async () =>
                {
                    IPrivilegiosColeccion db = new PrivilegiosColeccion();
                    privilegios = await db.GetAllPrivilegios();
                    return privilegios;
                });

                cmBoxPrivilegios.ItemsSource = privilegios;
            }
            catch (Exception ex)
            {

                MessageBox.Show($"{ex}");
            }


        }
        private async void DtRoles_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                await Task.Delay(TimeSpan.FromSeconds(0.5));

                // cmboDepartamento.Text = Nulos.DepartamentoJefe;

                Roles Nulos = (Roles)DtRoles.SelectedItem;


                if (Nulos != null)
                {
                    txtBoxIdRol.Text = Nulos.Id.ToString();
                    txtBoxNombreRol.Text = Nulos.NombreRol;


                }

            }
            catch (Exception ex)
            {
                MessageBox.Show($"{ex.Message}");
            }
        }

        private void Boton_LimpiarRoles(object sender, RoutedEventArgs e)
        {
            LimpiarControles();
        }

        private void TxtBuscarRol_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {

            if (TxtBuscarRol.Text.Equals(""))
            {
                RellenarGrid();

            }

            var filtered = rolesObservable
                .Where(x => x.NombreRol.ToLower().Trim() == TxtBuscarRol.Text.ToLower().Trim())
                .ToList();

            DtRoles.ItemsSource = filtered;
        }

        private void Buton_Refrescar(object sender, RoutedEventArgs e)
        {
            RellenarGrid();

        }

        #endregion

        #region CRUD
        private async void Buton_AgregarRol(object sender, RoutedEventArgs e)
        {
            try
            {
                errores.Clear();
                IRolesColeccion db = new RolesColeccion();
                Roles roles = new Roles();
                ArrayList privilegios = new ArrayList();


                roles.NombreRol = txtBoxNombreRol.Text;

                if (cmBoxPrivilegios.Text != null)
                {
                    foreach (var item in cmBoxPrivilegios.Items.OfType<Privilegios>())
                    {
                        bool isChecked = item.isChecked;
                        if (isChecked)
                        {
                            int contador = +1;
                            for (int i = 0; i < contador; i++)
                            {
                                privilegios.Add(item.NombrePrivilegio.ToString());
                            }
                        }
                    }

                    roles.Privilegios = privilegios;
                }

                var resultados = validationRules.Validate(roles);

                if (resultados.IsValid == false)
                {
                    foreach (var item in resultados.Errors)
                    {
                        DialogsValidators dialogsValidators = new DialogsValidators(item.ErrorMessage);
                        dialogsValidators.ShowDialog();
                    }
                }
                else
                {
                    await db.InsertRol(roles);
                    rolesObservable.Add(roles);

                    DialogsSucess dialogsSucess = new DialogsSucess($"El Rol {roles.NombreRol} ha sido registrado");
                    dialogsSucess.ShowDialog();

                    RellenarComboBox();
                    LimpiarControles();
                    

                    ISubject subjectEntities = new SubjectEntities(roles);
                    subjectEntities.Register(App.observerEntities);
                    App.observerEntities.ActualizarWindow();
                }

            }
            catch (Exception ex)
            {

                DialogsValidators dialogsValidators = new DialogsValidators(ex.Message);
                dialogsValidators.ShowDialog();
            }

        }
        private async void Boton_ActualizarRol(object sender, RoutedEventArgs e)
        {
            try
            {
                errores.Clear();

                IRolesColeccion db = new RolesColeccion();
                Roles roles = new Roles();
                ArrayList privilegios = new ArrayList();
                roles.NombreRol = txtBoxNombreRol.Text;

                if (cmBoxPrivilegios.Text != null)
                {
                    foreach (var item in cmBoxPrivilegios.Items.OfType<Privilegios>())
                    {
                        bool isChecked = item.isChecked;
                        if (isChecked)
                        {
                            int contador = +1;
                            for (int i = 0; i < contador; i++)
                            {
                                privilegios.Add(item.NombrePrivilegio.ToString());
                            }
                        }
                    }

                    roles.Privilegios = privilegios;
                }

                if (txtBoxIdRol.Text.Length > 0)
                {
                    roles.Id = ObjectId.Parse(txtBoxIdRol.Text);
                }
                else
                {
                    DialogsValidators dialogsValidators = new DialogsValidators("El Id no puede estar Vacío");
                    dialogsValidators.ShowDialog();
                }

       
                roles.NombreRol = txtBoxNombreRol.Text;

                var resultados = validationRules.Validate(roles);

                if (resultados.IsValid == false)
                {
                    foreach (var item in resultados.Errors)
                    {
                        DialogsValidators dialogsValidators = new DialogsValidators(item.ErrorMessage);
                        dialogsValidators.ShowDialog();
                    }
                }
                else
                {
                    await db.UpdateRol(roles);
                    var d = rolesObservable.Where(x => x.Id == ObjectId.Parse(txtBoxIdRol.Text)).First();
                    rolesObservable.Remove(d);
                    rolesObservable.Add(roles);

                    DialogsSucess dialogsSucess = new DialogsSucess($"El Rol {roles.NombreRol} ha sido actualizado");
                    dialogsSucess.ShowDialog();

                    RellenarComboBox();
                    LimpiarControles();

                    ISubject subjectEntities = new SubjectEntities(roles);
                    subjectEntities.Register(App.observerEntities);
                    App.observerEntities.ActualizarWindow();
                }

            }
            catch (Exception ex)
            {

                DialogsValidators dialogsValidators = new DialogsValidators(ex.Message);
                dialogsValidators.ShowDialog();
            }

        }
        private async void Boton_EliminarRol(object sender, RoutedEventArgs e)
        {
            try
            {
                IRolesColeccion db = new RolesColeccion();
                var resultados = validationRulesDelete.Validate(txtBoxIdRol.Text);

                if (resultados.IsValid == false)
                {
                    foreach (var item in resultados.Errors)
                    {
                        DialogsValidators dialogsValidators = new DialogsValidators(item.ErrorMessage);
                        dialogsValidators.ShowDialog();
                    }
                }
                else
                {
                    await db.DeleteRol(txtBoxIdRol.Text);
                    var d = rolesObservable.Where(x => x.Id == ObjectId.Parse(txtBoxIdRol.Text)).First();
                    rolesObservable.Remove(d);

                    DialogsSucess dialogsSucess = new DialogsSucess($"El Rol {d.NombreRol} ha sido borrado");
                    dialogsSucess.ShowDialog();

                    RellenarComboBox();
                    LimpiarControles();
                    
                    ISubject subjectEntities = new SubjectEntities(d);
                    subjectEntities.Register(App.observerEntities);
                    App.observerEntities.ActualizarWindow();
                }

            }
            catch (Exception ex)
            {

                DialogsValidators dialogsValidators = new DialogsValidators(ex.Message);
                dialogsValidators.ShowDialog();
            }

        }







        #endregion
    }
}
