﻿using control_tareas_escritorio.Model;
using control_tareas_escritorio.Repository.DepartamentoRepositorio;
using control_tareas_escritorio.Repository.RolesRepositorio;
using control_tareas_escritorio.Repository.UsuariosRepositorio;
using MongoDB.Bson;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using control_tareas_escritorio.Helpers;
using System.Linq;
using System.ComponentModel;
using control_tareas_escritorio.Validators;
using System.Collections.ObjectModel;
using control_tareas_escritorio.Dialogs;
using control_tareas_escritorio.Observer;

namespace control_tareas_escritorio.UserControls
{
    /// <summary>
    /// Lógica de interacción para UsuarioControl.xaml
    /// </summary>
    public partial class UsuarioControl : UserControl
    {
        BindingList<string> errores = new BindingList<string>();
        UsuariosValidator validationRules = new UsuariosValidator();
        UsuariosValidatorDelete validationRulesDelete = new UsuariosValidatorDelete();
        ObservableCollection<Usuario> usuariosObservable;

        public static UsuarioControl instance;
        public static UsuarioControl GetInstance()
        {
            if (instance == null)
            {
                instance = new UsuarioControl();

            }
            return instance;
        }
        public UsuarioControl()
        {
            InitializeComponent();
            instance = this;
            RellenarDataGrid();
            RellenarComboBoxDepartamento();
            RellenarComboBoxRoles();
        }


        #region Controles
        private async void RellenarDataGrid()
        {
            //IEnumerable usuarios = new List<Usuario>();

            await Task.Run(async () =>
            {
                IUsuarioColeccion db = new UsuarioColeccion();
                usuariosObservable = await db.GetAllUsuariosAutomatic();
                return usuariosObservable;
            });

            DtUsuarios.ItemsSource = usuariosObservable;

        }
        private async void RellenarComboBoxDepartamento()
        {

            IEnumerable departamentos = new List<Departamento>();

            await Task.Run(async () =>
            {
                IDepartamentoColeccion db = new DepartamentoColeccion();

                departamentos = await db.GetAllDepartmentsWithoutNulls();
                return departamentos;
            });

            cmBoxDepartamentos.ItemsSource = departamentos;
            cmBoxDepartamentos.DisplayMemberPath = "NombreDepartamento";

        }
        private async void RellenarComboBoxRoles()
        {
            IEnumerable roles = new List<Roles>();

            await Task.Run(async () =>
            {
                IRolesColeccion db = new RolesColeccion();

                roles = await db.GetAllRol();
                return roles;
            });

            cmBoxRoles.ItemsSource = roles;
            cmBoxRoles.DisplayMemberPath = "NombreRol";
        }
        private void LimpiarControles()
        {

            txtBoxIdUsuario.Text = "";
            txtBoxNombre.Text = "";
            txtBoxApellido.Text = "";
            txtBoxEmail.Text = "";
            txtBoxContraseñaUsuario.Password = "";
            cmBoxDepartamentos.Text = "";
            cmBoxRoles.Text = "";
        }
        private async void DtUsuarios_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                await Task.Delay(TimeSpan.FromSeconds(0.5));
                Usuario Nulos = (Usuario)DtUsuarios.SelectedItem;


                if (Nulos != null)
                {
                    txtBoxIdUsuario.Text = Nulos.Id.ToString();
                    txtBoxNombre.Text = Nulos.Nombre;
                    txtBoxApellido.Text = Nulos.Apellido;
                    txtBoxEmail.Text = Nulos.Correo;
                    txtBoxContraseñaUsuario.Password = Nulos.Contrasena;

                    cmBoxDepartamentos.Text = Nulos.Departamento;
                    if (Nulos.Roles != null)
                    {
                        string nombre = Nulos.Roles.NombreRol;
                        cmBoxRoles.Text = nombre;

                    }
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show($"{ex.Message}");
            }
        }

        private void Boton_LimpiarUsuario(object sender, RoutedEventArgs e)
        {
            LimpiarControles();
        }
        #endregion

        #region CRUD
        private async void Buton_AgregarUsuario(object sender, RoutedEventArgs e)
        {

            try
            {
                IUsuarioColeccion db = new UsuarioColeccion();
                ILoginHelper loginhelper = new LoginHelper();
                Usuario usuario = new Usuario();
                usuario.Nombre = txtBoxNombre.Text;
                usuario.Apellido = txtBoxApellido.Text;
                usuario.Correo = txtBoxEmail.Text;
                usuario.Contrasena = (txtBoxContraseñaUsuario.Password.Length > 0) ? txtBoxContraseñaUsuario.Password : "";
                usuario.Departamento = (cmBoxDepartamentos.Text.Length > 0) ? cmBoxDepartamentos.Text : "";

                if (cmBoxRoles.Text.Length > 0)
                {
                    Roles roles = new Roles();
                    IRolesColeccion MetodosRoles = new RolesColeccion();
                    ObjectId IdTemporal = (cmBoxRoles.SelectedItem as Roles).Id;
                    roles = await MetodosRoles.Lookup(IdTemporal);
                    usuario.Roles = roles;
                }

                var resultados = validationRules.Validate(usuario);

                if (resultados.IsValid == false)
                {
                    foreach (var item in resultados.Errors)
                    {
                        DialogsValidators dialogsValidators = new DialogsValidators(item.ErrorMessage);
                        dialogsValidators.ShowDialog();

                    }
                }
                else
                {
                    usuario.Contrasena = loginhelper.HashPassword(txtBoxContraseñaUsuario.Password);
                    await db.InsertUser(usuario);
                    usuariosObservable.Add(usuario);

                    DialogsSucess dialogsSucess = new DialogsSucess($"El usuario {usuario.Nombre} ha sido registrado" );
                    dialogsSucess.ShowDialog();

                    RellenarComboBoxDepartamento();
                    LimpiarControles();


                    ISubject subjectEntities = new SubjectEntities(usuario);
                    subjectEntities.Register(App.observerEntities);
                    App.observerEntities.ActualizarWindow();
                }

            }
            catch (Exception ex)
            {

                DialogsValidators dialogsValidators = new DialogsValidators(ex.Message);
                dialogsValidators.ShowDialog();
            }

        }
        private async void Boton_ActualizarUsuario(object sender, RoutedEventArgs e)
        {
            try
            {
                IUsuarioColeccion db = new UsuarioColeccion();
                Usuario usuario = new Usuario();
                ILoginHelper loginhelper = new LoginHelper();

                if (txtBoxIdUsuario.Text.Length > 0)
                {
                    usuario.Id = ObjectId.Parse(txtBoxIdUsuario.Text);
                }
                else
                {
                    DialogsValidators dialogsValidators = new DialogsValidators("El Id no puede estar Vacío");
                    dialogsValidators.ShowDialog();
                }


            
                usuario.Nombre = txtBoxNombre.Text;
                usuario.Apellido = txtBoxApellido.Text;
                usuario.Correo = txtBoxEmail.Text;
                usuario.Contrasena = (txtBoxContraseñaUsuario.Password.Length > 0) ? txtBoxContraseñaUsuario.Password : "";
                usuario.Departamento = (cmBoxDepartamentos.Text != null) ? cmBoxDepartamentos.Text : "";

                if (cmBoxRoles.Text.Length > 0)
                {
                    Roles roles = new Roles();
                    IRolesColeccion MetodosRoles = new RolesColeccion();
                    ObjectId IdTemporal = (cmBoxRoles.SelectedItem as Roles).Id;
                    roles = await MetodosRoles.Lookup(IdTemporal);
                    usuario.Roles = roles;
                }

                var resultados = validationRules.Validate(usuario);

                if (resultados.IsValid == false)
                {
                    foreach (var item in resultados.Errors)
                    {
                        DialogsValidators dialogsValidators = new DialogsValidators(item.ErrorMessage);
                        dialogsValidators.ShowDialog();
                    }
                }
                else
                {
                    usuario.Contrasena = loginhelper.HashPassword(txtBoxContraseñaUsuario.Password);
                    await db.UpdateUser(usuario);

                    var d = usuariosObservable.Where(x => x.Id == ObjectId.Parse(txtBoxIdUsuario.Text)).First();
                    usuariosObservable.Remove(d);
                    usuariosObservable.Add(usuario);

                    DialogsSucess dialogsSucess = new DialogsSucess($"El usuario {usuario.Nombre} ha sido actualizado");
                    dialogsSucess.ShowDialog();

                    RellenarComboBoxDepartamento();
                    LimpiarControles();

                    ISubject subjectEntities = new SubjectEntities(usuario);
                    subjectEntities.Register(App.observerEntities);
                    App.observerEntities.ActualizarWindow();
                }
            }
            catch (Exception ex)
            {

                DialogsValidators dialogsValidators = new DialogsValidators(ex.Message);
                dialogsValidators.ShowDialog();
            }



        }
        private async void Boton_EliminarUsuario(object sender, RoutedEventArgs e)
        {
            try
            {
                IUsuarioColeccion db = new UsuarioColeccion();
                var resultados = validationRulesDelete.Validate(txtBoxIdUsuario.Text);

                if (resultados.IsValid == false)
                {
                    foreach (var item in resultados.Errors)
                    {
                        DialogsValidators dialogsValidators = new DialogsValidators(item.ErrorMessage);
                        dialogsValidators.ShowDialog();
                    }
                }
                else
                {
                    await db.DeleteUser(txtBoxIdUsuario.Text);
                    var d = usuariosObservable.Where(x => x.Id == ObjectId.Parse(txtBoxIdUsuario.Text)).First();
                    usuariosObservable.Remove(d);

                    DialogsSucess dialogsSucess = new DialogsSucess($"El usuario {d.Nombre} ha sido borrado");
                    dialogsSucess.ShowDialog();

                    RellenarComboBoxDepartamento();
                    LimpiarControles();

                    ISubject subjectEntities = new SubjectEntities(d);
                    subjectEntities.Register(App.observerEntities);
                    App.observerEntities.ActualizarWindow();
                }
            }
            catch (Exception ex)
            {

                DialogsValidators dialogsValidators = new DialogsValidators(ex.Message);
                dialogsValidators.ShowDialog();
            }


        }



        #endregion

        private void Boton_LimpiarRoles(object sender, RoutedEventArgs e)
        {
            LimpiarControles();
        }

        private void TxtBuscarUsuario_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {

            if (TxtBuscarUsuario.Text.Equals(""))
            {
                RellenarDataGrid();

            }

            var filtered = usuariosObservable
                .Where(x => x.Nombre.ToLower().Trim() == TxtBuscarUsuario.Text.ToLower().Trim()
                || x.Apellido.ToLower().Trim() == TxtBuscarUsuario.Text.ToLower().Trim())
                .ToList();

            DtUsuarios.ItemsSource = filtered;
        }

        private void Buton_Refrescar(object sender, RoutedEventArgs e)
        {
             RellenarDataGrid();

        }
    }
}
