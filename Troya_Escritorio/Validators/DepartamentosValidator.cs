﻿using control_tareas_escritorio.Model;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace control_tareas_escritorio.Validators
{
    public class DepartamentosValidator : AbstractValidator<Departamento>
    {
        public DepartamentosValidator()
        {
            RuleFor(p => p.NombreDepartamento)
                .Cascade(CascadeMode.Stop)
                .NotEmpty().WithMessage("El Nombre Departamento esta vacío")
                .Length(3, 50).WithMessage("El nombre del departamento tiene que ser mayor a 3 caracteres y menor a 50 caracteres sin caracteres especiales");              
        }
    }
}
