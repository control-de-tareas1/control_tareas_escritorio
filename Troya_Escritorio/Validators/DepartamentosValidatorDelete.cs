﻿using control_tareas_escritorio.Model;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace control_tareas_escritorio.Validators
{
    public class DepartamentosValidatorDelete : AbstractValidator<string>
    {
        public DepartamentosValidatorDelete()
        {
            RuleFor(i => i)
                .Cascade(CascadeMode.Stop)
                .NotEmpty().WithMessage("El Id no puede estar vacío");
        }
    }
}
