﻿using control_tareas_escritorio.Model;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace control_tareas_escritorio.Validators
{
    public class PrivilegiosValidator : AbstractValidator<Privilegios> 
    {
        public PrivilegiosValidator()
        {
            RuleFor(p => p.NombrePrivilegio)
                .Cascade(CascadeMode.Stop)
                .NotEmpty().WithMessage("El Nombre esta vacío")
                .Length(3, 10).WithMessage("El nombre del privilegio tiene que ser mayor a 3 caracteres y menor a 50 caracteres")
                .Must(LetrasValidas).WithMessage("El Nombre del privilegio contiene caracteres inválidos");

        }

        protected bool LetrasValidas( string privilegio)
        {
            privilegio = privilegio.Replace(" ", "");
            privilegio = privilegio.Replace("-", "");
            return privilegio.All(Char.IsLetter);
        }
    }
}
