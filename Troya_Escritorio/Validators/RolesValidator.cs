﻿using control_tareas_escritorio.Model;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace control_tareas_escritorio.Validators
{
    public class RolesValidator : AbstractValidator<Roles>
    {
        public RolesValidator()
        {
            RuleFor(p => p.NombreRol)
                .Cascade(CascadeMode.Stop)
                .NotEmpty().WithMessage("El Nombre del Rol esta vacío")
                .Length(3, 50).WithMessage("El nombre del rol tiene que ser mayor a 3 caracteres y menor a 50 caracteres sin caracteres especiales ni números");

        }
    }
}
