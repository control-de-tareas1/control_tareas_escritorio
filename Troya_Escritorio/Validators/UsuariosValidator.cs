﻿using control_tareas_escritorio.Model;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace control_tareas_escritorio.Validators
{
    public class UsuariosValidator : AbstractValidator<Usuario>
    {
        public UsuariosValidator()
        {
            RuleFor(p => p.Nombre)
                    .Cascade(CascadeMode.Stop)
                    .NotEmpty().WithMessage("El Nombre del usuario esta vacío")
                    .Length(3, 50).WithMessage("El nombre del usuario tiene que ser mayor a 3 caracteres y menor a 50 caracteres sin caracteres especiales ni números");
           
            RuleFor(p => p.Apellido)
                  .Cascade(CascadeMode.Stop)
                  .NotEmpty().WithMessage("El Apellido del usuario esta vacío")
                  .Length(3, 50).WithMessage("El apellido del usuario tiene que ser mayor a 3 caracteres y menor a 50 caracteres sin caracteres especiales ni números");
            
            RuleFor(p => p.Correo)
                  .Cascade(CascadeMode.Stop)
                  .NotEmpty().WithMessage("El Email esta vacío")
                  .EmailAddress().WithMessage("Debe tener el formato de correo");
            
            RuleFor(p => p.Contrasena)
                  .Cascade(CascadeMode.Stop)
                  .NotEmpty().WithMessage("La contraseña del usuario esta vacía")
                  .Length(8,32).Matches(@"^(?:(?:(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]))|(?:(?=.*[a-z])(?=.*[A-Z])(?=.*[*.!@$%^&(){}[]:;<>,.?/~_+-=|\]))|(?:(?=.*[0-9])(?=.*[A-Z])(?=.*[*.!@$%^&(){}[]:;<>,.?/~_+-=|\]))|(?:(?=.*[0-9])(?=.*[a-z])(?=.*[*.!@$%^&(){}[]:;<>,.?/~_+-=|\]))).{8,32}$")
                  .WithMessage("Al menos 1 dígito, 1 carácter en minúscula, 1 Carácter en mayúscula, 1 Carácter especial");               
           

        }
    }
}
