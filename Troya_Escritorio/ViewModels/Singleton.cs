﻿using control_tareas_escritorio.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace control_tareas_escritorio.ViewModels
{
    public class Singleton : INotifyPropertyChanged
    {
        private static Singleton instance = null;

        private ObservableCollection<Privilegios> _lista = new ObservableCollection<Privilegios>();

        public ObservableCollection<Privilegios> Lista
        {
            get { return _lista; }
            set { _lista = value; OnPropertyChanged(); }
        }
        private Singleton()
        {

        }
        public static Singleton Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Singleton();
                }
                return instance;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
