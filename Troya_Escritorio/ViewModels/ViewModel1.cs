﻿using control_tareas_escritorio.Model;
using control_tareas_escritorio.Repository.PrivilegiosRepositorio;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace control_tareas_escritorio.ViewModels
{

    // Detectar cambios en las propiedades de los objetos 
    public class ViewModel1 : INotifyPropertyChanged
    {

        private ObservableCollection<Privilegios> _listaScreenPrivilegios;
        public ObservableCollection<Privilegios> ListaScreenPrivilegios
        {
            get { return Singleton.Instance.Lista; }
            set { _listaScreenPrivilegios = value; OnPropertyChanged(); }
        }

        public ViewModel1()
        {
            fill();
        }

        public async void fill()
        {

            ObservableCollection<Privilegios> ListaScreenPrivilegioss = new ObservableCollection<Privilegios>();

            await Task.Run(async () =>
            {
                IPrivilegiosColeccion db = new PrivilegiosColeccion();
                ListaScreenPrivilegioss = await db.GetAllPrivilegiossAutomatic();
                return ListaScreenPrivilegioss;
            });

            if (ListaScreenPrivilegios.Count <= 0)
            {
                foreach (var item in ListaScreenPrivilegioss)
                {
                    ListaScreenPrivilegios.Add(item);
                }
            }

        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }



}
