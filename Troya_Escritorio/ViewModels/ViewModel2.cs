﻿using control_tareas_escritorio.Model;
using control_tareas_escritorio.Repository.PrivilegiosRepositorio;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace control_tareas_escritorio.ViewModels
{
    public class ViewModel2 : INotifyPropertyChanged 
    {

        private ObservableCollection<Privilegios> _listaScreenDashboard;

        public ObservableCollection<Privilegios> ListaScreenDashboard
        {
            get
            {
                return Singleton.Instance.Lista;
            }
            set { _listaScreenDashboard = value; OnPropertyChanged(); }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

}
