﻿using control_tareas_escritorio.Cache;
using control_tareas_escritorio.Helpers;
using control_tareas_escritorio.Model;
using control_tareas_escritorio.Repository.UsuariosRepositorio;
using ControlzEx.Theming;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using SQLite;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

using System.Resources;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using control_tareas_escritorio.Repository.DepartamentoRepositorio;
using control_tareas_escritorio.Repository.RolesRepositorio;
using MongoDB.Bson;
using control_tareas_escritorio.Dialogs;
using System.ComponentModel;
using control_tareas_escritorio.Validators;
using System.Drawing;

namespace control_tareas_escritorio.Views
{
    /// <summary>
    /// Lógica de interacción para Login.xaml
    /// </summary>


    public partial class LoginView : MetroWindow
    {


        private Usuario usuario = null;
        private Usuario res;
        BindingList<string> errores = new BindingList<string>();
        UsuariosValidator validationRules = new UsuariosValidator();

        public static LoginView instance;
        public static LoginView GetInstance()
        {
            if (instance == null)
            {
                instance = new LoginView();

            }
            return instance;
        }
        public LoginView()
        {
            InitializeComponent();
            RellenarComboBoxDepartamento();
            Check();
            ColorsCombo.ItemsSource = ThemeManager.Current.ColorSchemes;
            ThemeCombo.ItemsSource = ThemeManager.Current.BaseColors;
            instance = this;

        }

        #region All Login

        private async void Login(object sender, RoutedEventArgs e)
        {
            var settings = new MetroDialogSettings
            {
                NegativeButtonText = "Cancelar",
                AnimateShow = true

            };

            var controller = await LoginView.GetInstance().ShowProgressAsync("Por favor espere", "Iniciando Sesión...", true, settings);
            controller.SetProgress(0);
            await Task.Delay(3000);
            controller.SetProgress(0.1);

            if (controller.IsCanceled)
            {
                await Task.Delay(1000);
                controller.SetProgress(1);
                await controller.CloseAsync();
                await this.ShowMessageAsync("Operación Cancelado", "Ha cancelado el inicio de sesión");
                return;
            }
            await Task.Delay(1000);
            controller.SetProgress(0.3);
            controller.SetMessage("Cargando...");
            await Task.Delay(500);
            controller.SetProgress(0.7);
            controller.SetCancelable(false);
            Autenticar(controller);


        }

        private async void Autenticar(ProgressDialogController controller)
        {
            ILoginHelper loginHelper = new LoginHelper();
            IUsuarioColeccion Metodos = new UsuarioColeccion();
            if (loginHelper.EmptyTextBox(txtBoxEmailUsuario.Text, txtBoxContraseñaUsuario.Password))
            {
                res = await Metodos.Authentication(txtBoxEmailUsuario.Text.Trim());
                if (res != null)
                {
                    bool isValidPassword = loginHelper.ValidatePassword(txtBoxContraseñaUsuario.Password.Trim(), res.Contrasena);
                    if (isValidPassword)
                    {
                        usuario = res;
                    }
                    else
                    {
                        await Task.Delay(900);
                        controller.SetProgress(1);
                        await controller.CloseAsync();
                        await BasicDialog("Campos Incorrectos", $"Contraseña Inválida");
                    }
                }
                else
                {
                    await Task.Delay(900);
                    controller.SetProgress(1);
                    await controller.CloseAsync();
                    await BasicDialog($"Campos Incorrectos", $"Correo { txtBoxEmailUsuario.Text } no existe");
                }

                if (usuario != null)
                {
                    foreach (string item in usuario.Roles.Privilegios)
                    {
                        var privilegio = item;
                        if (privilegio == "Acceso Total Escritorio")
                        {
                            
                            controller.SetProgress(1);
                            await Task.Delay(900);
                            await controller.CloseAsync();

                            await BasicDialog($"Bienvenido {usuario.Nombre} a Troya", $"Planifica y Organiza tu empresa");

                            var theme = ThemeManager.Current.DetectTheme(this);

                            ThemeManager.Current.ChangeTheme(MainWindow.GetInstance(), theme);

                            MainWindow.GetInstance().Show();
                            this.Hide();
                        }
   
                    }

                    controller.SetProgress(1);
                    await Task.Delay(900);
                    await controller.CloseAsync();
                    await BasicDialog($"Acceso Denegado", $"El usuario { usuario.Nombre} no tiene permisos. Intente Nuevamente");

                }

            }

            else
            {
                controller.SetProgress(1);
                await Task.Delay(900);
                await controller.CloseAsync();
                await BasicDialog($"Campos Incorrectos", $"Campos Vacíos");
            }


        }

        public async Task BasicDialog(string Text1, string Text2)
        {

            MetroDialogSettings Miconf = new MetroDialogSettings()
            {
                AffirmativeButtonText = "OK",
                ColorScheme = MetroDialogColorScheme.Accented
            };



            await GetInstance().ShowMessageAsync(Text1, Text2, MessageDialogStyle.Affirmative,
                Miconf);
        }

        #endregion

        #region Alto cotraste

        private void ColorsCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            var selectedValue = ColorsCombo.SelectedItem as string;
            if (selectedValue == "Blue")
                ThemeManager.Current.ChangeThemeColorScheme(this, selectedValue);
            if (selectedValue == "Red")
                ThemeManager.Current.ChangeThemeColorScheme(this, selectedValue);
            if (selectedValue == "Green")
                ThemeManager.Current.ChangeThemeColorScheme(this, selectedValue);
            if (selectedValue == "Purple")
                ThemeManager.Current.ChangeThemeColorScheme(this, selectedValue);
            if (selectedValue == "Orange")
                ThemeManager.Current.ChangeThemeColorScheme(this, selectedValue);
            if (selectedValue == "Lime")
                ThemeManager.Current.ChangeThemeColorScheme(this, selectedValue);
            if (selectedValue == "Emerald")
                ThemeManager.Current.ChangeThemeColorScheme(this, selectedValue);
            if (selectedValue == "Teal")
                ThemeManager.Current.ChangeThemeColorScheme(this, selectedValue);
            if (selectedValue == "Cyan")
                ThemeManager.Current.ChangeThemeColorScheme(this, selectedValue);
            if (selectedValue == "Cobalt")
                ThemeManager.Current.ChangeThemeColorScheme(this, selectedValue);
            if (selectedValue == "Indigo")
                ThemeManager.Current.ChangeThemeColorScheme(this, selectedValue);
            if (selectedValue == "Violet")
                ThemeManager.Current.ChangeThemeColorScheme(this, selectedValue);
            if (selectedValue == "Pink")
                ThemeManager.Current.ChangeThemeColorScheme(this, selectedValue);
            if (selectedValue == "Magenta")
                ThemeManager.Current.ChangeThemeColorScheme(this, selectedValue);
            if (selectedValue == "Crimson")
                ThemeManager.Current.ChangeThemeColorScheme(this, selectedValue);
            if (selectedValue == "Amber")
                ThemeManager.Current.ChangeThemeColorScheme(this, selectedValue);
            if (selectedValue == "Yellow")
                ThemeManager.Current.ChangeThemeColorScheme(this, selectedValue);
            if (selectedValue == "Brown")
                ThemeManager.Current.ChangeThemeColorScheme(this, selectedValue);
            if (selectedValue == "Olive")
                ThemeManager.Current.ChangeThemeColorScheme(this, selectedValue);
            if (selectedValue == "Steel")
                ThemeManager.Current.ChangeThemeColorScheme(this, selectedValue);
            if (selectedValue == "Mauve")
                ThemeManager.Current.ChangeThemeColorScheme(this, selectedValue);
            if (selectedValue == "Taupe")
                ThemeManager.Current.ChangeThemeColorScheme(this, selectedValue); ;
            if (selectedValue == "Sienna")
                ThemeManager.Current.ChangeThemeColorScheme(this, selectedValue);

        }

        private void ThemeCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var Light = ThemeManager.BaseColorLightConst;
            var Dark = ThemeManager.BaseColorDarkConst;
            var selectedValue = ThemeCombo.SelectedItem as string;
            if (selectedValue == "Dark")
                ThemeManager.Current.ChangeThemeBaseColor(this, Dark);


            if (selectedValue == "Light")
                ThemeManager.Current.ChangeThemeBaseColor(this, Light);
            ;
        }
        #endregion

        #region View

        private void MetroWindow_Closed(object sender, EventArgs e)
        {

            MainWindow.GetInstance().Close();

            this.Close();

            if (ToggleLogin.IsOn)
            {




                UserCache userCache = new UserCache();
                userCache.Mail = txtBoxEmailUsuario.Text;
                userCache.Password = txtBoxContraseñaUsuario.Password;
                userCache.IsChecked = true;
                userCache.Id = 1;

                using (SQLiteConnection connection = new SQLiteConnection(App.databasePath))
                {


                    connection.CreateTable<UserCache>();
                    var user = (connection.Table<UserCache>().ToList()).Where(c => c.Id == userCache.Id).FirstOrDefault();

                    if (user == null)
                    {
                        connection.InsertOrReplace(userCache);
                    }
                    else
                    {
                        connection.Update(userCache);


                    }

                }

            }
            else
            {
                UserCache userCache = new UserCache();
                userCache.Mail = "";
                userCache.Password = "";
                userCache.IsChecked = false;
            }



        }

        private void Check()
        {

            using (SQLiteConnection connection = new SQLiteConnection(App.databasePath))
            {
                connection.CreateTable<UserCache>();
                var user = connection.Table<UserCache>().FirstOrDefault(c => c.Id == 1);

                if (user != null)
                {
                    if (user.IsChecked)
                    {
                        txtBoxEmailUsuario.Text = user.Mail;
                        txtBoxContraseñaUsuario.Password = user.Password;
                        ToggleLogin.IsOn = true;
                    }
                }
            };






        }
        #endregion

        #region Registro


        private async void RellenarComboBoxDepartamento()
        {

            IEnumerable departamentos = new List<Departamento>();

            await Task.Run(async () =>
            {
                IDepartamentoColeccion db = new DepartamentoColeccion();

                departamentos = await db.GetAllDepartmentsWithoutNulls();
                return departamentos;
            });

            cmBoxDepartamentos.ItemsSource = departamentos;
            cmBoxDepartamentos.DisplayMemberPath = "NombreDepartamento";

        }

        private void LimpiarControles()
        {
            txtNombreUsuario.Text = "";
            txtApellidoUsuario.Text = "";
            txtEmailUsuario.Text = "";
            txtPasswordUsuario.Password = "";
            cmBoxDepartamentos.Text = "";
        }

        private async void Boton_Registrarse(object sender, RoutedEventArgs e)
        {

            try
            {
                IUsuarioColeccion db = new UsuarioColeccion();
                ILoginHelper loginhelper = new LoginHelper();
                Usuario usuario = new Usuario();
                usuario.Nombre = txtNombreUsuario.Text;
                usuario.Apellido = txtApellidoUsuario.Text;
                usuario.Correo = txtEmailUsuario.Text;
                usuario.Contrasena = (txtPasswordUsuario.Password.Length > 0) ? txtPasswordUsuario.Password : "";
                usuario.Departamento = (cmBoxDepartamentos.Text.Length > 0) ? cmBoxDepartamentos.Text : "";

                Roles roles = new Roles();
                IRolesColeccion MetodosRoles = new RolesColeccion();
                ObjectId IdTemporal = ObjectId.Parse("5f6971e4864bac4d584b8ed1");

                roles = await MetodosRoles.Lookup(IdTemporal);
                usuario.Roles = roles;

                var resultados = validationRules.Validate(usuario);

                if (resultados.IsValid == false)
                {
                    foreach (var item in resultados.Errors)
                    {
                        DialogsValidators dialogsValidators = new DialogsValidators(item.ErrorMessage);
                        dialogsValidators.ShowDialog();

                    }
                }
                else
                {
                    usuario.Contrasena = loginhelper.HashPassword(txtPasswordUsuario.Password.Trim());
                    await db.InsertUser(usuario);

                    DialogsSucess dialogsSucess = new DialogsSucess($"El usuario {usuario.Nombre} ha sido registrado");
                    dialogsSucess.ShowDialog();

                    LimpiarControles();

                }
            }
            catch (Exception ex)
            {

                DialogsValidators dialogsValidators = new DialogsValidators(ex.Message);
                dialogsValidators.ShowDialog();
            }


        }

        #endregion
    
    }
}
