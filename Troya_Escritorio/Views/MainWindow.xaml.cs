﻿using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using control_tareas_escritorio.UserControls;
using control_tareas_escritorio.Views;
using ControlzEx.Theming;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace control_tareas_escritorio
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
   

        //DashboardControl DashboardControl = new DashboardControl();
        //RolesControl RolesControl = new RolesControl();
        //UsuarioControl UsuarioControl = new UsuarioControl();
        //DepartamentoControl DepartamentoControl = new DepartamentoControl();

        public static MainWindow instance;
        public static MainWindow GetInstance()
        {
            if (instance == null)
            {
                instance = new MainWindow();

            }
            return instance;
        }
        public MainWindow()
        {
            InitializeComponent();
            instance = this;
           
        }
        private void ListViewMenu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = ListViewMenu.SelectedIndex;
            MoveCursorMenu(index);

            switch (index)
            {
                case 0:
                    GridPrincipal.Children.Clear();
                    GridPrincipal.Children.Add(DashboardControl.GetInstance());
                    break;
                case 1:
                    GridPrincipal.Children.Clear();
                    GridPrincipal.Children.Add(UsuarioControl.GetInstance());
                    break;
                case 2:
                    GridPrincipal.Children.Clear();
                    GridPrincipal.Children.Add(RolesControl.GetInstance());
                    break;
                case 3:
                    GridPrincipal.Children.Clear();
                    GridPrincipal.Children.Add(DepartamentoControl.GetInstance());
                    break;
                default:
                    break;
            }

        }
        private void MoveCursorMenu(int index)
        {
            TrainsitionigContentSlide.OnApplyTemplate();
            GridCursor.Margin = new Thickness(0, (2 + (60 * index)), 0, 0);
        }
        public async void ShowMessagesDialog(string title, string message)
        {
            
            var messageDialogSettings = new MetroDialogSettings
            {
                AffirmativeButtonText = "Aceptar",
                OwnerCanCloseWithDialog = false,
                ColorScheme = this.MetroDialogOptions.ColorScheme,
            };
            await this.ShowMessageAsync(title, message, MessageDialogStyle.Affirmative, messageDialogSettings);
        }
        private async void Logout(object sender, RoutedEventArgs e)
        {
            
            MessageDialogResult result = await DialogTwoOptions("Ok","Cancelar","Cerrar Sesión","¿Está seguro de cerrar sesión?");
            if (result == MessageDialogResult.Affirmative)
            {

                var theme = ThemeManager.Current.DetectTheme(this);

                ThemeManager.Current.ChangeTheme( LoginView.GetInstance() , theme);
                this.Hide();
                LoginView.GetInstance().Show();
 

            }

        }
        public async Task<MessageDialogResult> DialogTwoOptions(string positive, string negative, string Text1, string Text2)
        {
           
            //configura las opciones del dialogo
            var Miconf = new MetroDialogSettings()
            {
                AffirmativeButtonText = positive,
                NegativeButtonText = negative,
                ColorScheme = MetroDialogColorScheme.Accented,
                
                
               
            };

            var result =
            await this.ShowMessageAsync(
                Text1,
                Text2,
                MessageDialogStyle.AffirmativeAndNegative,
                Miconf
                );
            return result;
        }

        private void MetroWindow_Closed(object sender, System.EventArgs e)
        {
            this.Close();
            LoginView.GetInstance().Close();
        }

    }
}