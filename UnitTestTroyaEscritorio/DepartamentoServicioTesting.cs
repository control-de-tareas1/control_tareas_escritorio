using control_tareas_escritorio.Model;
using control_tareas_escritorio.Repository.DepartamentoRepositorio;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IntegrationTestingTroyaEscritorio 
{
    [TestClass]
    public class DepartamentoServicioTesting 
    {
        DepartamentoColeccion departamentoColeccion  = new DepartamentoColeccion();
        Departamento departamentoCrear = new Departamento();
        Departamento departamentoCrearConJefe = new Departamento();      

        #region Crear Departamentos



        // Crear Departamento sin Jefe
        [TestMethod, Priority(1)]
        public async Task CrearDepartamento_NombreDepartamentoTestAsync()
        {
            departamentoCrear.NombreDepartamento = "Biologia";
            await departamentoColeccion.InsertDepartment(departamentoCrear);

            List<Departamento> departamentos = await departamentoColeccion.GetAllDepartments();
            Departamento departamentoCreado = departamentos
                .Where(x => x.NombreDepartamento == departamentoCrear.NombreDepartamento)
                .FirstOrDefault<Departamento>();

            Assert.IsNotNull(departamentoCreado);
            Assert.AreEqual(departamentoCrear.NombreDepartamento, departamentoCreado.NombreDepartamento);
            Assert.IsNull(departamentoCreado.DepartamentoJefe);


            await ActualizarDepartamentoCreado();
        }

        // Crear Departamento con Jefe
        [TestMethod, Priority(2)] 
        public async Task CrearDepartamento_DepartamentoConJefeTestAsync() 
        {
            departamentoCrearConJefe.NombreDepartamento = "Alquimia";
            departamentoCrearConJefe.DepartamentoJefe   = "Milton";
            await departamentoColeccion.InsertDepartment(departamentoCrearConJefe);

            List<Departamento> departamentos = await departamentoColeccion.GetAllDepartments();
            Departamento departamentoCreado = departamentos
                .Where(x => x.NombreDepartamento == departamentoCrearConJefe.NombreDepartamento && x.DepartamentoJefe == departamentoCrearConJefe.DepartamentoJefe )
                .FirstOrDefault<Departamento>();

            Assert.IsNotNull(departamentoCreado);
            Assert.AreEqual(departamentoCrearConJefe.NombreDepartamento, departamentoCreado.NombreDepartamento);
            Assert.AreEqual(departamentoCrearConJefe.DepartamentoJefe,departamentoCreado.DepartamentoJefe);

           await ActualizarDepartamentoCreadoConJefe();
        }
        #endregion


        #region Obtener Departamentos

        /// Buscar todos los departamentos: Siempre y Cuando existan 
        [TestMethod, Priority(3)]
        public async Task ObtenerTodosDepartamentosTest()
        {
           List<Departamento> departamentos = await departamentoColeccion.GetAllDepartments();
           bool IsDepartamentos = (departamentos.Count > 0) ? true : false; 

           Assert.IsNotNull(departamentos);
           Assert.IsTrue(IsDepartamentos);
           Assert.AreEqual(departamentos.GetType(), typeof(List<Departamento>)); 
        }

        // Buscando Departamento aleatorio
        [TestMethod, Priority(4)]
        public async Task NotFoundDepartamentoTest()
        {
           List<Departamento> departamentos = await departamentoColeccion.GetAllDepartments();
           ObjectId id = ObjectId.GenerateNewId();
           Departamento departamento = departamentos.Where(x => x.Id == id).FirstOrDefault();
           
           Assert.IsNull(departamento);
        }

        //Buscando departamento Finanzas
        [TestMethod, Priority(5)]
        public async Task FoundDepartamentoTest() 
        {

            List<Departamento> departamentos = await departamentoColeccion.GetAllDepartments();
            ObjectId id = ObjectId.Parse("5f693ee55337f7fffdf37c7a");
            Departamento departamento = departamentos.Where(x => x.Id == id).FirstOrDefault();

            Assert.IsNotNull(departamento);
            Assert.AreEqual("Finanzas", departamento.NombreDepartamento);
            Assert.AreEqual("", departamento.DepartamentoJefe);
        }

        #endregion


        #region Actualizar Departamentos


        // Actualizar departamento creado
 
        public async Task ActualizarDepartamentoCreado()
        {
            List<Departamento> departamentosBuscar = await departamentoColeccion.GetAllDepartments();
            Departamento departamentoBuscar = departamentosBuscar.Where(x => x.NombreDepartamento == "Biologia").FirstOrDefault();

            departamentoBuscar.NombreDepartamento = "Biologiaa";

            await departamentoColeccion.UpdateDepartment(departamentoBuscar); 

            List<Departamento> departamentos = await departamentoColeccion.GetAllDepartments();
            Departamento departamentoActualizado= departamentos
                 .Where(x => x.NombreDepartamento == "Biologiaa")
                .FirstOrDefault<Departamento>();

            Assert.IsNotNull(departamentoActualizado);
            Assert.AreEqual(departamentoBuscar.NombreDepartamento, departamentoActualizado.NombreDepartamento);
            Assert.IsNull(departamentoActualizado.DepartamentoJefe);

        }


        // Actualizar departamento creado
        public async Task ActualizarDepartamentoCreadoConJefe()
        {
            List<Departamento> departamentosBuscar = await departamentoColeccion.GetAllDepartments();            
            Departamento departamentoBuscar = departamentosBuscar
                .Where(x => x.NombreDepartamento == "Alquimia" && x.DepartamentoJefe == "Milton")
                .FirstOrDefault();

            departamentoBuscar.NombreDepartamento = "Alquimiaa";
            departamentoBuscar.DepartamentoJefe = "Miltoon";

            await departamentoColeccion.UpdateDepartment(departamentoBuscar);

            List<Departamento> departamentos = await departamentoColeccion.GetAllDepartments();
            Departamento departamentoActualizado = departamentos
                 .Where(x => x.NombreDepartamento == "Alquimiaa" && x.DepartamentoJefe == "Miltoon")
                .FirstOrDefault<Departamento>();

            Assert.IsNotNull(departamentoActualizado);
            Assert.AreEqual(departamentoBuscar.NombreDepartamento, departamentoActualizado.NombreDepartamento);
            Assert.AreEqual(departamentoBuscar.DepartamentoJefe, departamentoActualizado.DepartamentoJefe);

        }
        #endregion
     
        
        #region Eliminar Departamentos


        // Eliminar Departamento Creado
        [TestMethod, Priority(8)]
        public async Task EliminarDepartamentoCreadoTest()
        {
            List<Departamento> departamentosBuscar = await departamentoColeccion.GetAllDepartments();
            Departamento departamentoBuscar  = departamentosBuscar.Where(x => x.NombreDepartamento == "Biologiaa").FirstOrDefault(); 

            await departamentoColeccion.DeleteDepartment(departamentoBuscar.Id.ToString());

            List<Departamento> departamentos = await departamentoColeccion.GetAllDepartments();
            Departamento departamentoEliminado  = departamentos
                .Where(x => x.Id == departamentoBuscar.Id)
                .FirstOrDefault<Departamento>();

            Assert.IsNull(departamentoEliminado);

        }



        // Eliminar Departamento Creado con jefe
        [TestMethod, Priority(9)]
        public async Task EliminarDepartamentoCreadoCOnJefeTest()
        {

            List<Departamento> departamentosBuscar = await departamentoColeccion.GetAllDepartments();
            Departamento departamentoBuscar = departamentosBuscar.Where(x => x.NombreDepartamento == "Alquimiaa" && x.DepartamentoJefe == "Miltoon").FirstOrDefault();

            await departamentoColeccion.DeleteDepartment(departamentoBuscar.Id.ToString());

            List<Departamento> departamentos = await departamentoColeccion.GetAllDepartments();
            Departamento departamentoEliminado = departamentos
                .Where(x => x.Id == departamentoBuscar.Id)
                .FirstOrDefault<Departamento>();

            Assert.IsNull(departamentoEliminado); 
        }

        #endregion

    

    
    }
}
