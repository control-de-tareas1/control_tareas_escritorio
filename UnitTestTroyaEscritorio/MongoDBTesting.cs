﻿using control_tareas_escritorio.Context;
using control_tareas_escritorio.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace IntegrationTestingTroyaEscritorio 
{

    [TestClass]
    public class MongoDBTesting
    {

        [TestMethod]

        public void ProbarConexion_ServersActivosTresTesting() 
        {
            MongoDBContexto mongoDBContexto = new MongoDBContexto("ControlTareas");
            int expected = mongoDBContexto.client.Cluster.Description.Servers.Count ;

            var db = mongoDBContexto.db.GetCollection<Departamento>("departamentos");
             
            Assert.AreEqual(3, expected);           
        }


        //[TestMethod]

        //public void ProbarConexion_ServerActivosCeroTesting()
        //{
        //    MongoDBContexto mongoDBContexto = new MongoDBContexto("******");
        //    int expected = mongoDBContexto.client.Cluster.Description.Servers.Count;

        //    Assert.AreEqual(0, expected);
        //}

        [TestMethod]

        public async Task ProbarConexionTestingAsync()
        {
            MongoDBContexto mongoDBContexto = new MongoDBContexto("ControlTareas");
        
            bool isMongoConnected;
            try
            {
                await mongoDBContexto.db.RunCommandAsync((Command<BsonDocument>)"{ping:1}");
                isMongoConnected = true;
            }
            catch (Exception)
            {
                isMongoConnected = false;
            }

            Assert.IsTrue(isMongoConnected);
        }

    }
}
