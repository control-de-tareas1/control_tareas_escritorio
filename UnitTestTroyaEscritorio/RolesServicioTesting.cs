﻿using control_tareas_escritorio.Model;
using control_tareas_escritorio.Repository.PrivilegiosRepositorio;
using control_tareas_escritorio.Repository.RolesRepositorio;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IntegrationTestingTroyaEscritorio 
{

    [TestClass]
    public class RolesServicioTesting
    {

        RolesColeccion rolesColeccion = new RolesColeccion();
        PrivilegiosColeccion privilegiosColeccion = new PrivilegiosColeccion();
        Roles rolCrear = new Roles();


        #region ObtenerRoles
        //Buscar Todos los Roles
        [TestMethod]
        public async Task ObtenerTodosLosPrivilegios()
        {
            List<Roles> Roles = await rolesColeccion.GetAllRol();
            bool IsRoles = (Roles.Count > 0) ? true : false;

            Assert.IsNotNull(Roles);
            Assert.IsTrue(IsRoles);
            Assert.AreEqual(Roles.GetType(), typeof(List<Roles>));           
        }
        // Buscando Rol aleatorio
        [TestMethod]
        public async Task NotFoundRolTest()
        {
            List<Roles> roles = await rolesColeccion.GetAllRol();
            ObjectId id = ObjectId.GenerateNewId();
            Roles rol = roles.Where(x => x.Id == id).FirstOrDefault();

            Assert.IsNull(rol);
        }

        //Buscando Rol Escritorio Acceso
        [TestMethod]
        public async Task FoundRolTest()
        { 
            List<Roles> roles  = await rolesColeccion.GetAllRol();
            ObjectId id = ObjectId.Parse("5f6971e4864bac4d584b8ed1");
            Roles rol = roles.Where(x => x.Id == id).FirstOrDefault();


            Assert.IsNotNull(rol);
            Assert.AreEqual("Administrador", rol.NombreRol);

        }

        #endregion

        #region CrearRoles

        [TestMethod]
        public async Task CrearRolSinPrivilegios()
        {
            rolCrear.NombreRol = "Superheroe";
            await rolesColeccion.InsertRol(rolCrear);

            List<Roles> roles = await rolesColeccion.GetAllRol();
            Roles rolCreado = roles
                .Where(x => x.NombreRol == rolCrear.NombreRol)
                .FirstOrDefault<Roles>();

            Assert.IsNotNull(rolCreado);
            Assert.AreEqual(rolCreado.NombreRol, rolCrear.NombreRol);
            Assert.IsNull(rolCreado.Privilegios);

            await ActualizarRolCreado();
        }


        [TestMethod]
        public async Task CrearRolConPrivilegios()
        {
            ArrayList privilegiosInsertar = new ArrayList();
            privilegiosInsertar.Add("Acceso Total Escritorio");

            rolCrear.NombreRol = "Cocinero";
            rolCrear.Privilegios = privilegiosInsertar;

            await rolesColeccion.InsertRol(rolCrear);

            List<Roles> roles = await rolesColeccion.GetAllRol();
            Roles rolCreado = roles
                .Where(x => x.NombreRol == rolCrear.NombreRol)
                .FirstOrDefault<Roles>();

            Assert.IsNotNull(rolCreado);
            Assert.AreEqual(rolCreado.NombreRol, rolCrear.NombreRol);
            Assert.IsNotNull(rolCreado.Privilegios);

            await ActualizarRolCreadoConPrivilegios();
           
        }

        #endregion

        #region ActualizarRoles

        public async Task ActualizarRolCreado()
        {
            List<Roles> rolesBuscar = await rolesColeccion.GetAllRol();
            Roles rolBuscar = rolesBuscar.Where(x => x.NombreRol == "Superheroe").FirstOrDefault();

            rolBuscar.NombreRol = "Villano";
            await rolesColeccion.UpdateRol(rolBuscar);

            List<Roles> roles =await rolesColeccion.GetAllRol();
            Roles rolActualizado = roles
                 .Where(x => x.NombreRol == "Villano")
                 .FirstOrDefault<Roles>();

            Assert.IsNotNull(rolActualizado);
            Assert.AreEqual(rolActualizado.NombreRol, rolActualizado.NombreRol);
            Assert.IsNull(rolActualizado.Privilegios);

        }

        public async Task ActualizarRolCreadoConPrivilegios() 
        {
            List<Roles> rolesBuscar = await rolesColeccion.GetAllRol();
            Roles rolBuscar = rolesBuscar
                .Where(x => x.NombreRol == "Cocinero")
                .FirstOrDefault();

            ArrayList privilegios = new ArrayList();
            privilegios.Add("Acceso Total Workflow");

            rolBuscar.NombreRol = "Chef";
            rolBuscar.Privilegios = privilegios;
            await rolesColeccion.UpdateRol(rolBuscar);

            List<Roles> roles = await rolesColeccion.GetAllRol();
            Roles rolActualizado = roles
                 .Where(x => x.NombreRol == "Chef")
                 .FirstOrDefault<Roles>();

            Assert.IsNotNull(rolActualizado);
            Assert.AreEqual(rolActualizado.NombreRol, rolActualizado.NombreRol);
            Assert.IsNotNull(rolActualizado.Privilegios);

        }



        #endregion

        #region EliminarRoles

        [TestMethod]

        public async Task EliminarRolActualizado()
        {

            List<Roles> rolesBuscar = await rolesColeccion.GetAllRol();
            Roles  rolBuscar = rolesBuscar.Where(x => x.NombreRol == "Villano").FirstOrDefault();

            await rolesColeccion.DeleteRol(rolBuscar.Id.ToString());

            List<Roles> roles = await rolesColeccion.GetAllRol();
            Roles rolEliminado = roles
                .Where(x => x.Id == rolBuscar.Id)
                .FirstOrDefault<Roles>();

            Assert.IsNull(rolEliminado);

        }

        [TestMethod]

        public async Task EliminarRolConPrivilegiosActualizado()
        {
            List<Roles> rolesBuscar = await rolesColeccion.GetAllRol();
            Roles rolBuscar = rolesBuscar.Where(x => x.NombreRol == "Chef").FirstOrDefault();

            await rolesColeccion.DeleteRol(rolBuscar.Id.ToString());

            List<Roles> roles = await rolesColeccion.GetAllRol();
            Roles rolEliminado = roles
                .Where(x => x.Id == rolBuscar.Id)
                .FirstOrDefault<Roles>();

            Assert.IsNull(rolEliminado);
        }

        #endregion

    }
}
