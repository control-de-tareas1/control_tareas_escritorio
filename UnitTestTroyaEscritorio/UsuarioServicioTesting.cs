﻿using control_tareas_escritorio.Model;
using control_tareas_escritorio.Repository.UsuariosRepositorio;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntegrationTestingTroyaEscritorio 
{
    [TestClass]
    public class UsuarioServicioTesting
    {

        UsuarioColeccion usuarioColeccion = new UsuarioColeccion();


        #region CrearUsuarios

        [TestMethod]
        public async Task CrearUsuarioTesting()
        {

            Usuario usuario       = new Usuario();
            Roles roles           = new Roles();
            ArrayList Privilegios = new ArrayList();

            Privilegios.Add("Acceso Total");
            roles.Id          = ObjectId.GenerateNewId();
            roles.NombreRol   = "Presidente";
            roles.Privilegios = Privilegios;

            usuario.Nombre       = "Alexander";
            usuario.Apellido     = "Ahumada";
            usuario.Correo       = "alexander@gmail.com";
            usuario.Contrasena   = "1234";
            usuario.Departamento = "Finanzas";
            usuario.Roles        =  roles;

            await usuarioColeccion.InsertUser(usuario);

            List<Usuario> ListaUsuario = await usuarioColeccion.GetAllUsers();
            Usuario usuarioCreado = ListaUsuario
                                            .Where(x => x.Correo == usuario.Correo)
                                            .FirstOrDefault<Usuario>();

            Assert.IsNotNull(usuarioCreado); 
            Assert.AreEqual(usuarioCreado.GetType(), typeof(Usuario));
            Assert.IsNotNull(usuarioCreado.Id);
            Assert.AreEqual(usuarioCreado.Contrasena, usuario.Contrasena);

            await ActualizarUsuarioTesting();
        }
        #endregion

        #region ObtenerUsuarios

        [TestMethod]

        public async Task ObtenerUsuariosTesting()
        {
            List<Usuario> usuarios = await usuarioColeccion.GetAllUsers();

            bool isUsuarios = usuarios.Count > 0 ? true : false; 

            Assert.IsNotNull(usuarios);
            Assert.IsTrue(isUsuarios);
            Assert.AreEqual(usuarios.GetType(), typeof(List<Usuario>));
        }

        #endregion

        #region ActualizarUsuarios

        public async Task ActualizarUsuarioTesting()
        {
            List<Usuario> usuarios = await usuarioColeccion.GetAllUsers();

            Usuario usuarioBuscar = usuarios
                                       .Where(x => x.Correo == "alexander@gmail.com")
                                       .FirstOrDefault<Usuario>();

            usuarioBuscar.Nombre   = "Rene";
            usuarioBuscar.Apellido = "Ahumada";
            usuarioBuscar.Correo   = "rene@gmail.com";

            await usuarioColeccion.UpdateUser(usuarioBuscar);
             
            Usuario usuarioActualizado = usuarios
                           .Where(x => x.Correo == "rene@gmail.com")
                           .FirstOrDefault<Usuario>();


            Assert.IsNotNull(usuarioActualizado);
            Assert.AreEqual(usuarioActualizado.Correo, "rene@gmail.com");
        }

        #endregion

        #region EliminarUsuarios

        [TestMethod]
        public async Task EliminarUsuarioActualizado()
        {
            List<Usuario> usuarios = await usuarioColeccion.GetAllUsers();

            Usuario usuarioBuscar = usuarios
                                       .Where(x => x.Correo == "rene@gmail.com")
                                       .FirstOrDefault<Usuario>();

            await usuarioColeccion.DeleteUser(usuarioBuscar.Id.ToString());

            List<Usuario> usuariosBorrados = await usuarioColeccion.GetAllUsers();

            Usuario usuarioBorrado = usuariosBorrados
                                       .Where(x => x.Correo == "rene@gmail.com")
                                       .FirstOrDefault<Usuario>();

            Assert.IsNull(usuarioBorrado);


        }



        #endregion
    }
}
